import { ZettelId } from '@memorize/types';

export const femaleTuringWinnersId: ZettelId = 20200418172819;
export const liskovId: ZettelId = 20200418173133;
export const allenId: ZettelId = 20200418173047;
export const goldwasserId: ZettelId = 20200418173235;

export const femaleTuringWinnersZettel = {
  id: femaleTuringWinnersId,
  tags: ['female'],
  references: [allenId, liskovId, goldwasserId],
  title: 'Female Turing Award winners',
  metadata: {
    directory: './zettelkasten',
    filenameWithExtension: 'female_turing_award_winners.md',
  },
};

export const liskovZettel = {
  id: liskovId,
  tags: ['data_abstraction', 'fault_tolerance', 'distributed_computing'],
  references: [femaleTuringWinnersId],
  title: 'Barbara Liskov',
  metadata: {
    directory: './zettelkasten',
    filenameWithExtension: 'barbara_liskov.md',
  },
};

export const allenZettel = {
  id: allenId,
  tags: ['optimizing_compilers'],
  references: [femaleTuringWinnersId],
  title: 'Frances E. Allen',
  metadata: {
    directory: './zettelkasten',
    filenameWithExtension: 'frances_e_allen.md',
  },
};

export const goldwasserZettel = {
  id: goldwasserId,
  tags: ['cryptography'],
  references: [],
  title: 'Shafi Goldwasser',
  metadata: {
    directory: './zettelkasten',
    filenameWithExtension: 'shafi_goldwasser.md',
  },
};
