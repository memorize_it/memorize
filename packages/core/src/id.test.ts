import { id } from './id';

describe('Testing Id', () => {
  describe('When given a String', () => {
    test('It should return the matching ID', () => {
      expect(id('1970-01-01 15:00:0')).toEqual(19700101150000);
    });
    test('It should return the matching ID', () => {
      expect(id('Thu Jan 02 2020 16:33:11')).toEqual(20200102163311);
    });
  });

  describe('When given a Date', () => {
    test('It should return the matching ID', () => {
      const date = new Date('Thu Jan 02 2020 16:33:11');
      expect(id(date)).toEqual(20200102163311);
    });
  });
});
