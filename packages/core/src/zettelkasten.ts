import {
  ZettelId,
  ZettelkastenInterface,
  ZettelInterface,
  ZettelWithErrorInterface,
} from '@memorize/types';
import { jaroWinklerDistance } from '@memorize/language';
import { version } from './version';

export function consolidateTags(zettels: ZettelInterface[]): string[] {
  return zettels.reduce((existingTags: string[], { tags }) => {
    return [
      ...existingTags,
      ...tags.filter((item) => existingTags.indexOf(item) < 0),
    ];
  }, []);
}

export function similarTags(
  tagCandidates: string[],
  zettelkasten: ZettelkastenInterface,
  jaroWinklerThreshold = 0.8,
): { [ley: string]: string[] } {
  const existingTags: string[] = zettelkasten.meta.tags;

  return tagCandidates.reduce(
    (tagMap: { [key: string]: string[] }, tag: string) => {
      const matchingTags =
        existingTags.indexOf(tag) >= 0
          ? [] // If tag already exists, no suggestion is proposed
          : existingTags.filter(
              (eTag) => jaroWinklerDistance(tag, eTag) >= jaroWinklerThreshold,
            );
      tagMap[tag] = matchingTags;
      return tagMap;
    },
    {},
  );
}

export function createZettelkasten({
  zettels,
  updatedAt = Date.now(),
}: {
  zettels: ZettelInterface[];
  updatedAt?: number;
}): ZettelkastenInterface {
  const ids = zettelIdToSet(zettels);
  const zettelsWithError = zettels.reduce(
    (errors: ZettelWithErrorInterface[], zettel: ZettelInterface) => {
      const { references } = zettel;
      const existingReferences = references.filter((rId) => ids.has(rId));
      if (existingReferences.length !== references.length) {
        zettel.references = existingReferences;
        errors.push({
          id: zettel.id,
          missingReferences: references.filter((rId) => !ids.has(rId)),
        });
      }
      return errors;
    },
    [],
  );

  return {
    meta: {
      updatedAt,
      tags: consolidateTags(zettels),
      version,
      zettelsWithError,
    },
    zettels,
  };
}

export function getRelatedZettels(
  zettelkasten: ZettelkastenInterface,
  id: ZettelId,
): ZettelInterface[] {
  const { references } = findZettel(zettelkasten, id);
  const relatedZettels = zettelkasten.zettels.filter(
    ({ id: zId, references: zReferences }) =>
      zId !== id && (zReferences.includes(id) || references.includes(zId)),
  );

  const relatedIds = [id, ...relatedZettels.map(({ id: zId }) => zId)];

  return relatedZettels.map((z) => ({
    ...z,
    references: z.references.filter((r) => relatedIds.includes(r)),
  }));
}

export function findZettel(
  { zettels }: ZettelkastenInterface,
  zId: ZettelId,
): ZettelInterface {
  const zettel = zettels.find(({ id }) => zId === id);

  if (!zettel) {
    throw new Error(
      `zettelkasten(findZettel): zettel with id "${zId}" doesn't exist on provided zettelkasten`,
    );
  }

  return zettel;
}

export function zettelIdToIndexMap(
  zettels: ZettelInterface[],
): Map<ZettelId, number> {
  return zettels.reduce((indexMap, { id }, index) => {
    indexMap.set(id, index);
    return indexMap;
  }, new Map<ZettelId, number>());
}

export function zettelIdToSet(zettels: ZettelInterface[]): Set<ZettelId> {
  return new Set(zettels.map(({ id }) => id));
}
