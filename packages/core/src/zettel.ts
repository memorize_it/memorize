export const IDRegexp = /\d{14}/gm;

export function sanitizeText(text: string, spaceReplacement = '_'): string {
  if (!text) {
    throw new Error('zettel(sanitizeText): empty string provided');
  }

  return text
    .toLocaleLowerCase()
    .replace(/[^a-zA-Z0-9\ ]/g, '')
    .trim()
    .replace(/\ /g, spaceReplacement);
}
