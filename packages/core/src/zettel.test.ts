import { sanitizeText } from './zettel';

describe('Testing Zttle', () => {
  describe('When I want to get the filename from the title', () => {
    const tests = [
      'My new long title',
      'My New Long Title',
      'My new long title !',
      '# My new long title !',
      '# My new long title ! ',
    ];
    const result = 'my_new_long_title';

    tests.forEach((t) => {
      describe(`And the filename is ${t}`, () => {
        test('It should return a sanitized filename', () => {
          expect(sanitizeText(t)).toEqual(result);
        });
      });
    });

    const testWithNumbers = '01 my new long title';
    describe(`And the filename is ${testWithNumbers} and contains a number`, () => {
      test('It should return a sanitized filename', () => {
        expect(sanitizeText(testWithNumbers)).toEqual('01_my_new_long_title');
      });
    });

    const testWithSpaceReplacements = tests[0];
    describe(`And the filename is ${testWithSpaceReplacements} and contains a number`, () => {
      test('It should return a sanitized filename', () => {
        expect(sanitizeText(testWithSpaceReplacements, '-')).toEqual(
          'my-new-long-title',
        );
      });
    });
  });
});
