import { format } from 'date-fns';
import { ZettelId } from '@memorize/types';

function id(date: string | Date): ZettelId {
  if (typeof date === 'string') {
    date = new Date(date);
  }

  return Number.parseInt(format(date, 'yyyyMMddHHmmss'));
}

export { id };
