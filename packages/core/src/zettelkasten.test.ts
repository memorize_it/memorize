import {
  similarTags,
  getRelatedZettels,
  createZettelkasten,
  zettelIdToIndexMap,
} from './zettelkasten';
import { ZettelkastenInterface, ZettelInterface } from '@memorize/types';
import {
  allenId,
  allenZettel,
  goldwasserId,
  femaleTuringWinnersId,
  femaleTuringWinnersZettel,
  goldwasserZettel,
  liskovZettel,
  liskovId,
} from './zettel.mocks';

describe('Testing Zettelkasten', () => {
  describe('When I want to get similar tags', () => {
    const zettelkasten: ZettelkastenInterface = createZettelkasten({
      zettels: [
        femaleTuringWinnersZettel,
        liskovZettel,
        allenZettel,
        goldwasserZettel,
      ],
    });

    describe('And I propose a tag Array', () => {
      const tags = ['data_abstaction', 'non_existing_tag', 'anotherTag'];
      const result = {
        data_abstaction: ['data_abstraction'],
        non_existing_tag: [],
        anotherTag: [],
      };
      test('It should return map with the similar options of every tag', () => {
        expect(similarTags(tags, zettelkasten)).toEqual(result);
      });
    });

    describe('And the tag already exists', () => {
      const tags = ['data_abstraction', 'non_existing_tag', 'anotherTag'];
      const result = {
        data_abstraction: [],
        non_existing_tag: [],
        anotherTag: [],
      };

      describe('And I propose a tag Array', () => {
        test('It should return map without suggestions for already existing tag', () => {
          expect(similarTags(tags, zettelkasten)).toEqual(result);
        });
      });
    });
  });

  describe('When I want to create a zettelkasten', () => {
    describe('And zettels are coherent', () => {
      const zettelkasten: ZettelkastenInterface = createZettelkasten({
        zettels: [
          liskovZettel,
          allenZettel,
          goldwasserZettel,
          femaleTuringWinnersZettel,
        ],
      });
      test('It should return a zettelkstan without errors', () => {
        expect(zettelkasten.meta.zettelsWithError.length).toEqual(0);
      });
    });

    describe('And zettels contain missing references', () => {
      const missingReferences = [1, 2, 3];
      const zettelkasten: ZettelkastenInterface = createZettelkasten({
        zettels: [
          liskovZettel,
          allenZettel,
          { ...goldwasserZettel, references: [...missingReferences, liskovId] },
          femaleTuringWinnersZettel,
        ],
      });

      test('It should return a zettelkstan with errors', () => {
        expect(zettelkasten.meta.zettelsWithError.length).toEqual(1);
        expect(zettelkasten.meta.zettelsWithError[0].id).toEqual(goldwasserId);
        expect(zettelkasten.meta.zettelsWithError[0].missingReferences).toEqual(
          missingReferences,
        );
      });
    });
  });

  describe('When I want to get related zettels', () => {
    const zettelkasten: ZettelkastenInterface = createZettelkasten({
      zettels: [
        liskovZettel,
        allenZettel,
        goldwasserZettel,
        femaleTuringWinnersZettel,
      ],
    });

    describe('And I provide a non existing Id', () => {
      test('It should get an error', () => {
        let message: string | undefined;

        try {
          getRelatedZettels(zettelkasten, 1);
        } catch (e) {
          message = e.message;
        }

        expect(message).toBeTruthy();
      });
    });

    describe('And I provide an Id connected to another zettels', () => {
      const relatedZettels: ZettelInterface[] = [
        liskovZettel,
        allenZettel,
        goldwasserZettel,
      ];

      test('It should get an array with related zettels', () => {
        return expect(
          getRelatedZettels(zettelkasten, femaleTuringWinnersId),
        ).toEqual(relatedZettels);
      });
    });

    describe('And I provide an Id connected to another zettels with outside connections', () => {
      const relatedZettels: ZettelInterface[] = [
        {
          ...femaleTuringWinnersZettel,
          references: [allenId],
        },
      ];

      test('It should get an array with related zettels', () => {
        return expect(getRelatedZettels(zettelkasten, allenId)).toEqual(
          relatedZettels,
        );
      });
    });

    describe('And I provide an Id connected to another zettels with single-side connections', () => {
      const relatedZettels: ZettelInterface[] = [
        {
          ...femaleTuringWinnersZettel,
          references: [goldwasserId],
        },
      ];

      test('It should get an array with related zettels', () => {
        return expect(getRelatedZettels(zettelkasten, goldwasserId)).toEqual(
          relatedZettels,
        );
      });
    });
  });

  describe('When I want to create zettel map to index', () => {
    const zettels: ZettelInterface[] = [
      liskovZettel,
      allenZettel,
      goldwasserZettel,
      femaleTuringWinnersZettel,
    ];

    test('It should return a map with the original array index', () => {
      const map = zettelIdToIndexMap(zettels);

      expect(map.get(liskovId)).toEqual(0);
      expect(map.get(allenId)).toEqual(1);
      expect(map.get(goldwasserId)).toEqual(2);
      expect(map.get(femaleTuringWinnersId)).toEqual(3);
    });
  });
});
