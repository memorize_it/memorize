# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.9.1](https://gitlab.com/memorize_it/memorize/compare/v0.9.0...v0.9.1) (2021-04-18)

**Note:** Version bump only for package @memorize/core





# [0.9.0](https://gitlab.com/memorize_it/memorize/compare/v0.8.1...v0.9.0) (2021-04-18)


### Features

* **core:** check for zettel errors on create zettelkasten ([053b091](https://gitlab.com/memorize_it/memorize/commit/053b0917c46111cc2520ad9273d38a2e18c47928))
* **core:** replace tagson markdown with links ([650559d](https://gitlab.com/memorize_it/memorize/commit/650559d9a22495f1c03d572117c081397037faab))





# [0.8.0](https://gitlab.com/memorize_it/memorize/compare/v0.7.2...v0.8.0) (2020-12-27)


### Features

* **core:** add files with error to zettelkasten ([f89f9ef](https://gitlab.com/memorize_it/memorize/commit/f89f9ef4dc8f9bc5e00acf52142d0af29d90e059))
* **core:** find zettel in zettelkasten ([8285bf9](https://gitlab.com/memorize_it/memorize/commit/8285bf92e4902ab91c96e8538800aa766667acfc))





## [0.7.2](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.2) (2020-09-21)


### Features

* **core:** add version number to zettelkasten ([4eed9f3](https://gitlab.com/memorize_it/memorize/commit/4eed9f3cd21990f69669d4019c19679529df5c54))
* **core:** replace links on zettels, create zettelkasten and get relatedZettels ([6befa40](https://gitlab.com/memorize_it/memorize/commit/6befa40403a8fea8c431c237769a406947ef6b67))





## [0.7.1](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.1) (2020-09-21)


### Features

* **core:** add version number to zettelkasten ([4eed9f3](https://gitlab.com/memorize_it/memorize/commit/4eed9f3cd21990f69669d4019c19679529df5c54))
* **core:** replace links on zettels, create zettelkasten and get relatedZettels ([6befa40](https://gitlab.com/memorize_it/memorize/commit/6befa40403a8fea8c431c237769a406947ef6b67))





# [0.6.0](https://gitlab.com/memorize_it/memorize/compare/v0.5.0...v0.6.0) (2020-08-07)


### Features

* **core:** specify character to replace space when sanitizing text ([88d1003](https://gitlab.com/memorize_it/memorize/commit/88d1003752d5c82e71895f347924b9c7babd60f4))





# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)

**Note:** Version bump only for package @memorize/core





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **core:** add method to zettelkasten to consolidate tags ([9ad3e12](https://gitlab.com/memorize_it/memorize/commit/9ad3e129f9502ff7baa7d0920f53ca5b5e1f8ef3))





## [0.3.1](https://gitlab.com/memorize_it/memorize/compare/v0.3.0...v0.3.1) (2020-02-08)


### Bug Fixes

* **types:** use @memorize/types as dependency to allow use of enums,maps... on runtime ([58c64b0](https://gitlab.com/memorize_it/memorize/commit/58c64b06160c105ac9073e82de8b4d087883fe68))





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)


### Bug Fixes

* **core:** move regeps from types to core. They are used on the runtime !!! XD ([c613128](https://gitlab.com/memorize_it/memorize/commit/c6131288b5c50d6962e8c59ee6f4df03a4d4c60d))


### Features

* **core:** allow to search for similar  tags already exisitng ([984c478](https://gitlab.com/memorize_it/memorize/commit/984c47881fa4a32499fcb84f31bc07d44e03d7d3))
* **core:** wHen parsing similartags don't make any suggestion if tag already exists ([ae843b9](https://gitlab.com/memorize_it/memorize/commit/ae843b97e913167e99e14dc5d8a6cf853d14de16))





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **graph:** zettelkasten to graph funtion ([41df447](https://gitlab.com/memorize_it/memorize/commit/41df447ce26cd8d4220bd5815a40c1054e21edf9))
* **zettel:** add sanitize  text method ([fb0cfcf](https://gitlab.com/memorize_it/memorize/commit/fb0cfcf7c35a342d4494c24adf89da79038217ec))





## [0.1.1](https://gitlab.com/memorize_it/memorize/compare/v0.1.0...v0.1.1) (2020-01-18)

**Note:** Version bump only for package @memorize/core





# 0.1.0 (2020-01-18)


### Features

* **cli:** add cli with create id feature ([02fa676](https://gitlab.com/memorize_it/memorize/commit/02fa67638c6745cea73d2026e187b9b9980102bc))
* **core:** create id from date ([85d59c2](https://gitlab.com/memorize_it/memorize/commit/85d59c2a74173d1c3ed224c9e9ee1b7295d3ed4c))
