import { ZettelId } from '@memorize/types';
import { LinkInterface } from '../types/graph';

function referenceToLink(
  idToIndex: Map<ZettelId, number>,
  index: number,
  id: ZettelId,
): LinkInterface {
  const target = idToIndex.get(id);

  if (target === undefined) {
    throw new Error(`referenceToLink: can't find zettel with id: ${id}`);
  }

  return {
    source: index,
    target,
    value: 1,
  };
}

export default referenceToLink;
