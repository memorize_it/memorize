import { zettelIdToIndexMap } from '@memorize/core';
import { ZettelkastenInterface } from '@memorize/types';
import { GraphInterface } from '../../types/graph';
import referenceToLink from '../referenceToLink';

export function zettelkastenToForceDiagramGraph({
  zettels,
  meta,
}: ZettelkastenInterface): GraphInterface {
  const idToIndex = zettelIdToIndexMap(zettels);
  return zettels.reduce(
    (g: GraphInterface, { id, title, references, tags }, index) => {
      g.nodes.push({
        index,
        id,
        title,
        group: tags.length ? meta.tags.indexOf(tags[0]) : meta.tags.length + 1,
      });

      g.links.push(
        ...references.map((r) => referenceToLink(idToIndex, index, r)),
      );

      return g;
    },
    {
      nodes: [],
      links: [],
    },
  );
}
