import { Spec } from 'vega';
import { ZettelkastenInterface } from '@memorize/types';
import { zettelkastenToForceDiagramGraph } from './force.map';

export function zettelkastenToForceDiagram(z: ZettelkastenInterface): Spec {
  const { nodes, links } = zettelkastenToForceDiagramGraph(z);
  return {
    width: 700,
    height: 500,
    padding: 0,
    autosize: 'none',

    signals: [
      { name: 'cx', update: 'width / 2' },
      { name: 'cy', update: 'height / 2' },
      {
        name: 'nodeRadius',
        value: 8,
      },
      {
        name: 'nodeCharge',
        value: -30,
      },
      {
        name: 'linkDistance',
        value: 100,
      },
      { name: 'static', value: true },
      {
        description: 'State variable for active node fix status.',
        name: 'fix',
        value: false,
        on: [
          {
            events: 'symbol:mouseout[!event.buttons], window:mouseup',
            update: 'false',
          },
          {
            events: 'symbol:mouseover',
            update: 'fix || true',
          },
          {
            events: '[symbol:mousedown, window:mouseup] > window:mousemove!',
            update: 'xy()',
            force: true,
          },
        ],
      },
      {
        description: 'Graph node most recently interacted with.',
        name: 'node',
        value: null,
        on: [
          {
            events: 'symbol:mouseover',
            update: 'fix === true ? item() : node',
          },
        ],
      },
      {
        description: 'Flag to restart Force simulation upon data changes.',
        name: 'restart',
        value: false,
        on: [{ events: { signal: 'fix' }, update: 'fix && fix.length' }],
      },
      {
        name: 'clicked',
        on: [{ events: '@nodes:click', update: '{value: datum}', force: true }],
      },
    ],

    data: [
      {
        name: 'link-data',
        values: links,
      },
      {
        name: 'link-data-target',
        source: 'link-data',
        transform: [
          {
            type: 'aggregate',
            groupby: ['target'],
          },
        ],
      },
      {
        name: 'node-data',
        values: nodes,
        transform: [
          {
            type: 'lookup',
            from: 'link-data-target',
            key: 'target',
            fields: ['index'],
            as: ['targetDegree'],
            default: {
              count: 0,
            },
          },
        ],
      },
    ],

    scales: [
      {
        name: 'color',
        type: 'ordinal',
        domain: { data: 'node-data', field: 'group' },
        range: { scheme: 'category20c' },
      },
    ],

    marks: [
      {
        name: 'nodes',
        type: 'symbol',
        zindex: 1,

        from: { data: 'node-data' },
        on: [
          {
            trigger: 'fix',
            modify: 'node',
            values:
              'fix === true ? {fx: node.x, fy: node.y} : {fx: fix[0], fy: fix[1]}',
          },
          {
            trigger: '!fix',
            modify: 'node',
            values: '{fx: null, fy: null}',
          },
        ],

        encode: {
          enter: {
            fill: { scale: 'color', field: 'group' },
            stroke: { value: 'white' },
          },
          update: {
            size: {
              signal:
                '2 * nodeRadius * nodeRadius * (datum.targetDegree.count + 1)',
            },
            cursor: { value: 'pointer' },
          },
        },

        transform: [
          {
            type: 'force',
            iterations: 300,
            restart: { signal: 'restart' },
            static: { signal: 'static' },
            signal: 'force',
            forces: [
              { force: 'center', x: { signal: 'cx' }, y: { signal: 'cy' } },
              { force: 'collide', radius: { signal: 'nodeRadius' } },
              { force: 'nbody', strength: { signal: 'nodeCharge' } },
              {
                force: 'link',
                links: 'link-data',
                distance: { signal: 'linkDistance' },
              },
            ],
          },
        ],
      },
      {
        type: 'text',
        interactive: false,
        from: { data: 'nodes' },
        encode: {
          enter: {
            align: { value: 'center' },
            fontSize: { value: 15 },
            text: { field: 'datum.title' },
          },
          update: {
            x: { field: 'x' },
            y: { field: 'y', offset: -20 },
          },
        },
      },
      {
        type: 'path',
        from: { data: 'link-data' },
        interactive: false,
        encode: {
          update: {
            stroke: { value: '#ccc' },
            strokeWidth: { value: 0.5 },
          },
        },
        transform: [
          {
            type: 'linkpath',
            require: { signal: 'force' },
            shape: 'line',
            sourceX: 'datum.source.x',
            sourceY: 'datum.source.y',
            targetX: 'datum.target.x',
            targetY: 'datum.target.y',
          },
        ],
      },
    ],
  };
}
