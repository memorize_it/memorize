import { zettelIdToIndexMap } from '@memorize/core';
import { ZettelkastenInterface } from '@memorize/types';
import { GraphInterface } from '../../types/graph';
import referenceToLink from '../referenceToLink';

export function zettelkastenToArcDiagramGraph({
  zettels,
}: ZettelkastenInterface): GraphInterface {
  const idToIndex = zettelIdToIndexMap(zettels);

  return zettels.reduce(
    (g: GraphInterface, { id, title, references }, index) => {
      g.nodes.push({
        index,
        id,
        title,
        group: 1,
      });

      g.links.push(
        ...references.map((r) => referenceToLink(idToIndex, index, r)),
      );

      return g;
    },
    {
      nodes: [],
      links: [],
    },
  );
}
