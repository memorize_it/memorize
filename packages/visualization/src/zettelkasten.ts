import {
  ZettelkastenInterface,
  ZettelkastenVisualizationEnum,
  ZettelkastenVisualizationType,
} from '@memorize/types';
import { View, parse, Spec } from 'vega';
// import { zettelkastenToEdgeBundelingDiagram } from './diagrams/edge-bundeling/edge-bundeling.diagram';
import { zettelkastenToArcDiagram } from './diagrams/arc/arc.diagram';
import { zettelkastenToForceDiagram } from './diagrams/force/force.diagram';

export function getZettelkastenVisualizationSpec(
  zettelkasten: ZettelkastenInterface,
  type: ZettelkastenVisualizationType,
): Spec {
  switch (type) {
    case ZettelkastenVisualizationEnum.FORCE:
      return zettelkastenToForceDiagram(zettelkasten);
    case ZettelkastenVisualizationEnum.ARC:
      return zettelkastenToArcDiagram(zettelkasten);
    case ZettelkastenVisualizationEnum.EDGE:
      // spec = zettelkastenToEdgeBundelingDiagram(zettelkasten);
      throw new Error(
        'zettelkasten(getZettelkastenSpec): EDGE has not been implemented yet',
      );
    default:
      throw new Error(
        "zettelkasten(getZettelkastenSpec): Specified visualizaiton type doesn't estist",
      );
  }
}

export function getZettelkastenVisualization(
  zettelkasten: ZettelkastenInterface,
  type: ZettelkastenVisualizationType,
): View {
  return new View(parse(getZettelkastenVisualizationSpec(zettelkasten, type)), {
    renderer: 'none',
  });
}

export async function getZettelkastenVisualizationSvg(
  zettelkasten: ZettelkastenInterface,
  type: ZettelkastenVisualizationType,
): Promise<string> {
  return getZettelkastenVisualization(zettelkasten, type).toSVG();
}

export async function getZettelkastenVisualizationCanvas(
  zettelkasten: ZettelkastenInterface,
  type: ZettelkastenVisualizationType,
): Promise<HTMLCanvasElement> {
  return getZettelkastenVisualization(zettelkasten, type).toCanvas();
}
