import { ZettelId } from '@memorize/types';

export interface NodeInterface {
  index: number;
  id: ZettelId;
  title: string;
  group: number;
}

export interface LinkInterface {
  source: number;
  target: number;
  value: number;
}

export interface GraphInterface {
  nodes: NodeInterface[];
  links: LinkInterface[];
}
