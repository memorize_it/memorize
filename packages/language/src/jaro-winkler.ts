import { jaroDistance } from './jaro';

export function jaroWinklerDistance(
  s1: string,
  s2: string,
  scalingFactor = 0.1,
  matchingPrefix = 0,
): number {
  const jaro = jaroDistance(s1, s2);

  while (s1[matchingPrefix] === s2[matchingPrefix] && matchingPrefix < 4) {
    matchingPrefix++;
  }

  return Number.parseFloat(
    (jaro + matchingPrefix * scalingFactor * (1 - jaro)).toFixed(3),
  );
}
