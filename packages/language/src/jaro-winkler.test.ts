import { jaroWinklerDistance } from './jaro-winkler';
import { examples } from './examples';

describe('Testing Jaro Winkler distance', () => {
  describe('When I provide the same string', () => {
    test('It should return 1', () => {
      expect(jaroWinklerDistance('jaro-winler', 'jaro-winler')).toEqual(1);
    });
    describe('And one of the stirng contains the other', () => {
      test('It should NOT return 1', () => {
        expect(jaroWinklerDistance('jaro', 'jaro-winler')).not.toEqual(0);
      });
    });
  });

  describe('When I provide two empty strings', () => {
    test('It should return 1', () => {
      expect(jaroWinklerDistance('', '')).toEqual(1);
    });
  });

  describe('When I provide an empty string as first parameter', () => {
    test('It should return 0', () => {
      expect(jaroWinklerDistance('', 'jaro-winler')).toEqual(0);
    });
  });

  describe('When I provide an empty string as second parameter', () => {
    test('It should return 0', () => {
      expect(jaroWinklerDistance('jaro-winkler', '')).toEqual(0);
    });
  });

  describe('When I test the names used on the original study', () => {
    examples.forEach(({ s1, s2, jaroWinkler }) => {
      test(`'${s1}', '${s2}', should return '${jaroWinkler}'`, () => {
        expect(jaroWinklerDistance(s1, s2)).toEqual(jaroWinkler);
      });
    });
  });
});
