# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.9.1](https://gitlab.com/memorize_it/memorize/compare/v0.9.0...v0.9.1) (2021-04-18)

**Note:** Version bump only for package @memorize/utils





# [0.9.0](https://gitlab.com/memorize_it/memorize/compare/v0.8.1...v0.9.0) (2021-04-18)

**Note:** Version bump only for package @memorize/utils





# [0.8.0](https://gitlab.com/memorize_it/memorize/compare/v0.7.2...v0.8.0) (2020-12-27)

**Note:** Version bump only for package @memorize/utils





## [0.7.2](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.2) (2020-09-21)


### Features

* **utils:** add method to remove file ([99c497d](https://gitlab.com/memorize_it/memorize/commit/99c497df1ab47be6c837629e435add41c5bd5c67))
* **utils:** add new promisified methods for fs ([03dba15](https://gitlab.com/memorize_it/memorize/commit/03dba155243eb2242956056b7ccf50bcacdfd96a))





## [0.7.1](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.1) (2020-09-21)


### Features

* **utils:** add method to remove file ([99c497d](https://gitlab.com/memorize_it/memorize/commit/99c497df1ab47be6c837629e435add41c5bd5c67))
* **utils:** add new promisified methods for fs ([03dba15](https://gitlab.com/memorize_it/memorize/commit/03dba155243eb2242956056b7ccf50bcacdfd96a))





# [0.6.0](https://gitlab.com/memorize_it/memorize/compare/v0.5.0...v0.6.0) (2020-08-07)

**Note:** Version bump only for package @memorize/utils





# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)

**Note:** Version bump only for package @memorize/utils





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)

**Note:** Version bump only for package @memorize/utils





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)

**Note:** Version bump only for package @memorize/utils





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **utils:** add new utils module ([ebb2bbb](https://gitlab.com/memorize_it/memorize/commit/ebb2bbb6403433c697c1c3487e0bf3838d21624f))
