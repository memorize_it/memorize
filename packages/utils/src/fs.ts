import { promisify } from 'util';
import {
  exists,
  mkdir,
  writeFile,
  readFile,
  readdir,
  symlink,
  copyFile,
  stat,
  open,
  write,
  close,
  unlink,
} from 'fs';

export const existsAsync = promisify(exists);
export const mkdirAsync = promisify(mkdir);
export const readdirAsync = promisify(readdir);
export const readFileAsync = promisify(readFile);
export const writeFileAsync = promisify(writeFile);
export const removeFileAsync = promisify(unlink);
export const copyFileAsync = promisify(copyFile);
export const symlinkAsync = promisify(symlink);
export const statAsync = promisify(stat);
export const writeAsync = promisify(write);
export const openAsync = promisify(open);
export const closeAsync = promisify(close);
