// eslint-disable-next-line @typescript-eslint/ban-types
export function hasOwnProperty<X extends object, Y extends PropertyKey>(
  obj: X,
  prop: Y,
): obj is X & Record<Y, unknown> {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

// eslint-disable-next-line @typescript-eslint/ban-types
export function hasOwnArrayProperty<X extends object, Y extends PropertyKey>(
  obj: X,
  prop: Y,
): obj is X & Record<Y, Array<unknown>> {
  return hasOwnProperty(obj, prop) ? Array.isArray(obj[prop]) : false;
}
