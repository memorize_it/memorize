# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.9.1](https://gitlab.com/memorize_it/memorize/compare/v0.9.0...v0.9.1) (2021-04-18)

**Note:** Version bump only for package @memorize/types





# [0.9.0](https://gitlab.com/memorize_it/memorize/compare/v0.8.1...v0.9.0) (2021-04-18)


### Features

* **types:** add zettels with error to zettelkasten metadata ([389835d](https://gitlab.com/memorize_it/memorize/commit/389835d876fd38748dacaeaf21a8e66985192b55))





# [0.8.0](https://gitlab.com/memorize_it/memorize/compare/v0.7.2...v0.8.0) (2020-12-27)


### Features

* **types:** add error files to zettelkasten meta ([776a361](https://gitlab.com/memorize_it/memorize/commit/776a361dfc60eaa812b4526e2b41bd3aa7c5f3a8))





## [0.7.2](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.2) (2020-09-21)


### Features

* **types:** add optional content to zettel ([5263dc5](https://gitlab.com/memorize_it/memorize/commit/5263dc52cc8afe6585e4ad9275db9c0fb10cde16))
* **types:** add version to zettelkasten meta ([c113520](https://gitlab.com/memorize_it/memorize/commit/c11352018ac5c2b9a2b0621e911ef4d5937fefd3))
* **types:** specify zettel metadata ([48f28d2](https://gitlab.com/memorize_it/memorize/commit/48f28d24cceca4816bf8887f2d3df1aeb445d69f))





## [0.7.1](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.1) (2020-09-21)


### Features

* **types:** add optional content to zettel ([5263dc5](https://gitlab.com/memorize_it/memorize/commit/5263dc52cc8afe6585e4ad9275db9c0fb10cde16))
* **types:** add version to zettelkasten meta ([c113520](https://gitlab.com/memorize_it/memorize/commit/c11352018ac5c2b9a2b0621e911ef4d5937fefd3))
* **types:** specify zettel metadata ([48f28d2](https://gitlab.com/memorize_it/memorize/commit/48f28d24cceca4816bf8887f2d3df1aeb445d69f))





# [0.6.0](https://gitlab.com/memorize_it/memorize/compare/v0.5.0...v0.6.0) (2020-08-07)

**Note:** Version bump only for package @memorize/types





# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)

**Note:** Version bump only for package @memorize/types





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **types:** add tagsto zettelkasten metadata ([29b5e18](https://gitlab.com/memorize_it/memorize/commit/29b5e18162aea533b2d03e372051b047cf29ea6c))





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)


### Bug Fixes

* **core:** move regeps from types to core. They are used on the runtime !!! XD ([c613128](https://gitlab.com/memorize_it/memorize/commit/c6131288b5c50d6962e8c59ee6f4df03a4d4c60d))


### Features

* **types:** define diagram types ([a67c142](https://gitlab.com/memorize_it/memorize/commit/a67c142bab52a6a1c09d6e3445dbd8aba134b076))





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **types:** add new types module ([ea1f418](https://gitlab.com/memorize_it/memorize/commit/ea1f418e4bba629ca5172f7b1d32ed42bafb7d59))
* **zettel:** differentiate zettelbody and metadata ([dbd07a4](https://gitlab.com/memorize_it/memorize/commit/dbd07a4f76057e1aa3b21ab53dc5c49c9984adae))
