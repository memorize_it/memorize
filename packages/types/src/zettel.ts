export type ZettelId = number;

export interface ZettelBodyInterface {
  id: ZettelId;
  references: ZettelId[];
  tags: string[];
  title: string;
  content?: string;
}

export interface ZettelMetadataInterface {
  directory: string; // Path to file
  filenameWithExtension: string;
}

export interface ZettelInterface extends ZettelBodyInterface {
  metadata: ZettelMetadataInterface;
}

export interface ZettelWithContentInterface extends ZettelInterface {
  content: string;
  relatedZettels: ZettelInterface[];
}
