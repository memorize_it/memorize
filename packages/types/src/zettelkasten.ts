import {
  ZettelId,
  ZettelInterface,
  ZettelWithContentInterface,
} from './zettel';

export interface ZettelkastenCommonMetaInterface {
  updatedAt: number;
  tags: string[];
  version: string;
  zettelsWithError: ZettelWithErrorInterface[];
}

export interface ZettelkastenCommonInterface {
  meta: ZettelkastenCommonMetaInterface;
  zettels: ZettelInterface[] | ZettelWithContentInterface[];
}

export interface ZettelWithErrorInterface {
  id: ZettelId;
  missingReferences: ZettelId[];
}

export interface ZettelkastenInterface extends ZettelkastenCommonInterface {
  zettels: ZettelInterface[];
}

export interface ZettelkastenWithContentInterface
  extends ZettelkastenCommonInterface {
  zettels: ZettelWithContentInterface[];
}
