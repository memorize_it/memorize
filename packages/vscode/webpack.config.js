const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

/**@type {import('webpack').Configuration}*/
module.exports = {
  target: 'node',
  externals: {
    vscode: 'commonjs vscode',
    canvas: 'canvas',
  },
  entry: {
    main: './dist/extension.js',
  },
  output: {
    path: path.resolve(__dirname, 'out'),
    filename: 'extension.js',
    libraryTarget: 'commonjs2',
    devtoolModuleFilenameTemplate: '../[resource-path]',
  },
  plugins: [new CleanWebpackPlugin()],
  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.json'],
    symlinks: true,
  },
};
