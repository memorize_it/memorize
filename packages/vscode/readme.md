# Memorize - VSCode

This is the Visual Studio Code extension of [Memorize](https://memorize_it.gitlab.io/memorize/). 

📓 Memorize is a set of tools that allows you to use markdown files as a Zettelkasten note-taking system.

## Background

The idea behind Memorize :

- Use regular markdown files.
- Respect a minimum schema. No more than 10 convention to be respected.
- Do not depend on any specific software to **edit** the files. Use the editor of your choice.
- Provide tools that check the files and help to respect the conventions.
- Provide tools to explore the information in an interactive way.


### 💡 How Memorize works :

Memorize build a zettelkasten interpreting all the Markdown files inside a directory.

It will parse both, the front-matter (in `yaml`) and the content of the file.

For example, given this file :

```markdown
---
id: 20200224175325
tags:
  - tag
  - example_tag
references: []
---
# Example zettle

My markdown #IsCool
```

Memorize would build the following object :

```json
{
    "id": "20200120160015",
    "references": [],
    "tags": ["tag", "example_tag", "iscool"],
    "title": "Example zettle",
}
```

If for example, you want to make a link between two zettels you just need to include the zettelId inside the markdown body or in the references array.


For example, given this new file :

```markdown
---
id: 20200224175326
tags:
  - tag
references: 
  - 20200120160010
---
# Second zettle

- [Example zettel](20200120160015)
```

Memorize would build the following object :

```json
{
    "id": "20200120160016",
    "references": ["20200120160010", "20200120160015"],
    "tags": ["tag"],
    "title": "Second zettle",
}
```

## Features

Current features :

- [X] Generate new Id
- [X] Display graph visualization
- [X] Link autocompletion
- [X] Tag autocompletion
- [X] Link zettel from Id
- [X] Create new zettel
- [X] Interactive graph visualization - Click to open zettel 

Future features :

- [ ] Tag visualization

## Commands

- Memorize: New zettel ➡️ Create new zettel
- Memorize: Generate id ➡️ Generate id on the cursor position
- Memorize: Open zettel ➡️ Open zettel providing the ID or the path
- Memorize: Add zettel header to current file ➡️ Prepend zettel header on the current file
- Memorize: Visualize ➡️ Display graph visualization of the zettelkasten
- Memorize: Generate Zettels for empty links ➡️ Generates one new zettel for every empty links and replaces link target with zettel Id

## Requirements

At this moment there is no specific requirements other than VSCode.

## Extension Settings

Zettel filename space replacement : The character used to replace spaces when creating a new Zettel.
Zettelkasten visualization background : The background color for the zettelkasten visualization.
