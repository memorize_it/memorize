# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.9.1](https://gitlab.com/memorize_it/memorize/compare/v0.9.0...v0.9.1) (2021-04-18)

**Note:** Version bump only for package @memorize/vscode





# [0.9.0](https://gitlab.com/memorize_it/memorize/compare/v0.8.1...v0.9.0) (2021-04-18)


### Features

* **vscode:** extract selection to new zettel ([c7bdb3e](https://gitlab.com/memorize_it/memorize/commit/c7bdb3e63d9c4649c0a5f0f1cf10ce7519c2e414))
* **vscode:** let user configure if zettelkasten file is saved ([16e1fae](https://gitlab.com/memorize_it/memorize/commit/16e1fae5df8b8b0a19e11e00e720db1d23c8e920))





## [0.8.1](https://gitlab.com/memorize_it/memorize/compare/v0.8.0...v0.8.1) (2020-12-30)

**Note:** Version bump only for package @memorize/vscode





# [0.8.0](https://gitlab.com/memorize_it/memorize/compare/v0.7.2...v0.8.0) (2020-12-27)


### Features

* **vscode:** display and log errors ([2d29b52](https://gitlab.com/memorize_it/memorize/commit/2d29b5235ea3e4b3d1de80c0a5b43e805b0a42a4))





## [0.7.2](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.2) (2020-09-21)


### Features

* **vscode:** add zettel header on current file ([fa8655e](https://gitlab.com/memorize_it/memorize/commit/fa8655e528b2155ecdee9294f13eff068aa5a17d))
* **vscode:** create zettels for empty links ([51bb00d](https://gitlab.com/memorize_it/memorize/commit/51bb00d30e794d192045b240f5331e79938d5d2d))
* **vscode:** new command to Open Zettel given an ID or path ([d793c37](https://gitlab.com/memorize_it/memorize/commit/d793c37cca66242bd321ffff7c6b9334f69e830e))
* **vscode:** on the visualization open zettel on click ([9ade7dd](https://gitlab.com/memorize_it/memorize/commit/9ade7dd89f0fcf2253ec9b0b53c6d91c3a31f2c7))
* **vscode:** set background color for the visualization and default to white ([5af049b](https://gitlab.com/memorize_it/memorize/commit/5af049b3876de7240e1f95b74a56cef0ec5e211b))





## [0.7.1](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.1) (2020-09-21)


### Features

* **vscode:** add zettel header on current file ([fa8655e](https://gitlab.com/memorize_it/memorize/commit/fa8655e528b2155ecdee9294f13eff068aa5a17d))
* **vscode:** create zettels for empty links ([51bb00d](https://gitlab.com/memorize_it/memorize/commit/51bb00d30e794d192045b240f5331e79938d5d2d))
* **vscode:** new command to Open Zettel given an ID or path ([d793c37](https://gitlab.com/memorize_it/memorize/commit/d793c37cca66242bd321ffff7c6b9334f69e830e))
* **vscode:** on the visualization open zettel on click ([9ade7dd](https://gitlab.com/memorize_it/memorize/commit/9ade7dd89f0fcf2253ec9b0b53c6d91c3a31f2c7))
* **vscode:** set background color for the visualization and default to white ([5af049b](https://gitlab.com/memorize_it/memorize/commit/5af049b3876de7240e1f95b74a56cef0ec5e211b))





# [0.6.0](https://gitlab.com/memorize_it/memorize/compare/v0.5.0...v0.6.0) (2020-08-07)


### Features

* **vscode:** after creatingnew zettel open file and move to EOF ([10c7f95](https://gitlab.com/memorize_it/memorize/commit/10c7f95c5a8d60d73efaa29044ded95e7aa84442))
* **vscode:** use extensions setting to define a character to replace space on filename while creati ([032b65c](https://gitlab.com/memorize_it/memorize/commit/032b65c9fe8135d984d0ed08afbac08e7dab4826))





# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)


### Features

* **vscode:** add new zettel ([9e6514d](https://gitlab.com/memorize_it/memorize/commit/9e6514d7454334300648d95a8db986fce2d7c9fc))





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **vscode:** add tag and link completion ([57bb437](https://gitlab.com/memorize_it/memorize/commit/57bb437b615d0627aefff2716817b0af945fff2a))
* **vscode:** bootstrap vscode extension with generateId ([29f7e22](https://gitlab.com/memorize_it/memorize/commit/29f7e222c082a740f4bc74ec1ba85344d5630f0f))
* **vscode:** follow zettel id link ([d8d6235](https://gitlab.com/memorize_it/memorize/commit/d8d623582e9da28e05641efde57a4ea22cfe0c49))
* **vscode:** visualize  zettelkasten as svg ([e1228f9](https://gitlab.com/memorize_it/memorize/commit/e1228f9cb7ffc6c88e284277caa8ba4afb10f70c))
