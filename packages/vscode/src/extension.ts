// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import {
  ExtensionContext,
  commands,
  WebviewPanel,
  languages,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import {
  generateId,
  createNewZettel,
  askForPathAndOpenZettel,
  addZettelHeaderToCurrentFile,
  generateZettelsForEmptyLinks,
  extractToZettel,
} from './commands/zettel';
import { visualizeZettelkasten } from './commands/visualization';
import { linkCompletionProvider } from './languages/link-completion';
import { tagCompletionProvider } from './languages/tag-completion';
import { linkDocumentProvider } from './languages/link-document';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: ExtensionContext): void {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "Memorize" is now active!');

  const generateIdCmd = commands.registerCommand(
    'extension.generateId',
    generateId,
  );

  const currentPanel: WebviewPanel | undefined = undefined;
  const visualizeCmd = commands.registerCommand('extension.visualize', () =>
    visualizeZettelkasten(context, currentPanel),
  );

  const newZettelCmd = commands.registerCommand('extension.newZettel', () =>
    createNewZettel(),
  );

  const openZettelCmd = commands.registerCommand('extension.openZettel', () =>
    askForPathAndOpenZettel(),
  );

  const addZettelHeaderToCurrentFileCmd = commands.registerCommand(
    'extension.addZettelHeader',
    () => addZettelHeaderToCurrentFile(),
  );

  const generateZettelsForEmptyLinksInZettelCmd = commands.registerCommand(
    'extension.generateZettelsForEmptyLinksInZettel',
    () => generateZettelsForEmptyLinks(),
  );

  const extractToZettelCmd = commands.registerCommand(
    'extension.extractToZettel',
    () => extractToZettel(),
  );

  const linkAutocompletion = languages.registerCompletionItemProvider(
    { scheme: 'file', language: 'markdown' },
    linkCompletionProvider,
    '[',
  );

  const tagAutocompletion = languages.registerCompletionItemProvider(
    { scheme: 'file', language: 'markdown' },
    tagCompletionProvider,
    '#',
  );

  const zettelLink = languages.registerDocumentLinkProvider(
    { scheme: 'file', language: 'markdown' },
    linkDocumentProvider,
  );

  const cmds = [
    generateIdCmd,
    visualizeCmd,
    newZettelCmd,
    openZettelCmd,
    addZettelHeaderToCurrentFileCmd,
    generateZettelsForEmptyLinksInZettelCmd,
    extractToZettelCmd,
    linkAutocompletion,
    tagAutocompletion,
    zettelLink,
  ];

  context.subscriptions.push(...cmds);
}

// this method is called when your extension is deactivated
// export function deactivate() {}
