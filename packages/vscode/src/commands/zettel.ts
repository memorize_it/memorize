import {
  window,
  workspace,
  Position,
  Selection,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { id, sanitizeText } from '@memorize/core';
import {
  addZettelHeaderToFile,
  writeZettel,
  zettelIdToPath,
  generateZettelsForEmptyLinksInZettel,
  extractNewZettelFromSelection,
} from '@memorize/file-system';
import {
  getFilenameSpaceReplacement,
  getSaveZettelkastenFile,
} from '../config';
import { getRootPath, showInputBox, showErrorMessage } from './vscode-utils';

export function generateId(): void {
  try {
    const editor = window.activeTextEditor;

    if (!editor) {
      const message = 'open a file and try again.';
      throw new Error(message);
    }

    editor.edit((e) =>
      e.insert(editor.selection.active, id(new Date()).toString()),
    );
  } catch (err) {
    const message = 'Generate Id:';
    handleError(err, message);
  }
}

export async function openZettel(zettelIdOrFullPath: string): Promise<void> {
  try {
    const fullpath: string = Number.isNaN(zettelIdOrFullPath)
      ? zettelIdOrFullPath
      : await zettelIdToPath(
          getRootPath(),
          Number.parseInt(zettelIdOrFullPath),
          getSaveZettelkastenFile(),
        );
    const document = await workspace.openTextDocument(fullpath);
    await window.showTextDocument(document);

    const lastLine = document.lineAt(document.lineCount - 1);
    const position = new Position(lastLine.lineNumber + 1, 0);
    const selection = new Selection(position, position);
    const editor = window.activeTextEditor;
    if (!editor) {
      return;
    }
    editor.selections = [selection];
  } catch (err) {
    const message = 'Open Zettel:';
    handleError(err, message);
  }
}

export async function askForPathAndOpenZettel(): Promise<void> {
  try {
    const pathOrId = await showInputBox(
      'Enter ID or path',
      'Zettel ID or absolute path to the zettel',
      '',
    );

    if (!pathOrId) {
      const message = 'you must provide a path or an ID';
      throw new Error(message);
    }

    return openZettel(pathOrId);
  } catch (err) {
    const message = 'Open Zettel:';
    handleError(err, message);
  }
}

export async function createNewZettel(): Promise<void> {
  try {
    const directory = await showInputBox(
      'Enter path',
      'Path to you zettelkasten directory',
      getRootPath(),
    );

    if (!directory) {
      const message = 'you must provide a path';
      throw new Error(message);
    }

    const title = await showInputBox(
      'Enter the title of your new zettel',
      'Your new zettel title',
    );
    if (!title) {
      const message = 'you must provide a title';
      throw new Error(message);
    }

    const filename = await showInputBox(
      'Enter filename',
      'The filename for your new zettel',
      sanitizeText(title, getFilenameSpaceReplacement()),
    );
    if (!filename) {
      const message = 'you must provide a filename';
      throw new Error(message);
    }

    const fullpath = await writeZettel({
      id: id(new Date()),
      metadata: {
        filenameWithExtension: `${filename}.md`,
        directory,
      },
      references: [],
      tags: [],
      title,
    });
    return openZettel(fullpath);
  } catch (err) {
    const message = 'New Zettel: zettel not created';
    handleError(err, message);
  }
}

export async function addZettelHeaderToCurrentFile(): Promise<void> {
  try {
    if (!window.activeTextEditor) {
      const message = 'open a file and try again.';
      throw new Error(message);
    }

    const fullpath = window.activeTextEditor.document.uri.fsPath;
    if (!fullpath) {
      const message = 'open a file and try again.';
      throw new Error(message);
    }

    await addZettelHeaderToFile(fullpath);
    return openZettel(fullpath);
  } catch (err) {
    const message =
      'Add Zettel Header To Current File: header could not be added';
    handleError(err, message);
  }
}

export async function generateZettelsForEmptyLinks(): Promise<void> {
  try {
    const zettelPath = window.activeTextEditor?.document.uri.fsPath;
    if (!zettelPath) {
      const message = 'open a file and try again.';
      throw new Error(message);
    }

    await generateZettelsForEmptyLinksInZettel(
      zettelPath,
      getFilenameSpaceReplacement(),
    );
  } catch (err) {
    const message =
      'Generate Zettels for Empty Links: links could not be generated';
    handleError(err, message);
  }
}

function handleError(err: Error, message: string): void {
  console.error(err);
  const errorStack = [message];
  errorStack.push(err.message ? err.message : 'unknown error');
  showErrorMessage(errorStack);
}

export async function extractToZettel(): Promise<void> {
  try {
    const activeTextEditor = window.activeTextEditor;
    if (!activeTextEditor) {
      const message = 'no active text editor.';
      throw new Error(message);
    }
    const selection = activeTextEditor.selection;
    if (!selection) {
      const message = 'select the part to extract before.';
      throw new Error(message);
    }
    const zettelPath = activeTextEditor.document.uri.fsPath;
    if (!zettelPath) {
      const message = 'open a file and try again.';
      throw new Error(message);
    }

    const selectedText = activeTextEditor.document.getText(selection);
    const replacementText = await extractNewZettelFromSelection(
      selectedText,
      zettelPath,
    );

    activeTextEditor.edit((builder) =>
      builder.replace(selection, replacementText),
    );
  } catch (err) {
    const message = 'Extract to Zettel: could not extract to zettel';
    handleError(err, message);
  }
}
