import {
  window,
  WebviewPanel,
  ViewColumn,
  ExtensionContext,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { loadZettelkasten } from '@memorize/file-system';
import { getZettelkastenVisualizationSpec } from '@memorize/visualization';
import { ZettelkastenVisualizationEnum } from '@memorize/types';
import { openZettel } from './zettel';
import { getRootPath } from './vscode-utils';
import { getSaveZettelkastenFile, getVisualizationBackground } from '../config';

function getWebviewContent(backgroundColor?: string): string {
  return `<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">

      <link
        rel="stylesheet"
        href="https://unpkg.com/vega-tooltip@0.23.2/build/vega-tooltip.min.css"
      />
  
      <script src="https://unpkg.com/d3"></script>
      <script src="https://unpkg.com/vega/build/vega-core.min.js"></script>
    </head>
    <body style="background-color:${backgroundColor || 'white'};">
      <div id="example-row-vega" style="width: 500px; height: 500px;"></div>
  
      <script>
          (function() {
            const vscode = acquireVsCodeApi();

            window.addEventListener('message', (event) => {
              if (!event || !event.data) {
                return;
              }
              
              const spec = event.data;
              var view = new vega.View(vega.parse(spec))
              .renderer('svg') // set renderer (canvas or svg)
              .initialize('#example-row-vega') // initialize view within parent DOM container
              .hover() // enable hover encode set processing
              .run(); // run the dataflow and render the view
    
              d3.select(view.container())
                .select('svg')
                .style('width', view.width())
                .style('height', view.height());
      
              view.addSignalListener('clicked', (sig, val) => {
                vscode.postMessage({
                  command: 'clicked',
                  data: val.value.id,
                });
              });
              return;
            });
            
            vscode.postMessage({
              command: 'load',
            });
          }())
      </script>
    </body>
  </html>`;
}

export async function visualizeZettelkasten(
  context: ExtensionContext,
  currentPanel: WebviewPanel | undefined,
): Promise<void> {
  const rootPath = getRootPath();
  if (!rootPath) {
    return;
  }

  const columnToShowIn = window.activeTextEditor
    ? window.activeTextEditor.viewColumn
      ? window.activeTextEditor.viewColumn
      : ViewColumn.Beside
    : ViewColumn.Beside;

  if (currentPanel) {
    // If we already have a panel, show it in the target column
    currentPanel.reveal(columnToShowIn);
    return;
  }

  // Create and show a new webview
  currentPanel = window.createWebviewPanel(
    'visualization', // Identifies the type of the webview. Used internally
    `Zettelkasten visualization - ${rootPath}`, // Title of the panel displayed to the user
    columnToShowIn, // Editor column to show the new webview panel in.
    {
      enableScripts: true,
    }, // Webview options. More on these later.
  );

  currentPanel.webview.onDidReceiveMessage(
    async (message) => {
      if (message.command === 'load') {
        if (!currentPanel) {
          return;
        }

        const zettelkasten = await loadZettelkasten(
          rootPath,
          getSaveZettelkastenFile(),
        );
        const spec = getZettelkastenVisualizationSpec(
          zettelkasten,
          ZettelkastenVisualizationEnum.FORCE,
        );
        currentPanel.webview.postMessage(spec);
        return;
      }

      if (message.command === 'clicked') {
        return openZettel(message.data);
      }

      throw new Error('visualization(visualizeZettelkasten): unknown command');
    },
    undefined,
    context.subscriptions,
  );

  currentPanel.webview.html = getWebviewContent(getVisualizationBackground());

  currentPanel.onDidDispose(
    () => {
      // When the panel is closed, cancel any future updates to the webview content
      currentPanel = undefined;
    },
    null,
    context.subscriptions,
  );
}
