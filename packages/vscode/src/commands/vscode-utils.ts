import {
  window,
  workspace,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';

export async function showInputBox(
  prompt: string,
  placeHolder: string,
  defaultValue?: string,
): Promise<string | undefined> {
  const input = await window.showInputBox({
    prompt,
    placeHolder,
    value: defaultValue,
  });

  return input;
}

function messageToString(message: string | string[]): string {
  return typeof message === 'string' ? message : message.join('\n');
}

export function showErrorMessage(message: string | string[]): void {
  window.showErrorMessage(messageToString(message));
}

export function showInformationMessage(message: string | string[]): void {
  window.showInformationMessage(messageToString(message));
}

export function getRootPath(): string {
  return workspace.workspaceFolders && workspace.workspaceFolders.length
    ? workspace.workspaceFolders[0].uri.fsPath
    : '';
}
