// eslint-disable-next-line import/no-unresolved
import { workspace } from 'vscode';

const APP_NAME = 'memorize';

function getFilenameSpaceReplacement(): string | undefined {
  return workspace
    .getConfiguration(APP_NAME)
    .get('zettelFilenameSpaceReplacement');
}

function getVisualizationBackground(): string | undefined {
  return workspace
    .getConfiguration(APP_NAME)
    .get('zettelkastenVisualizationBackground');
}

function getSaveZettelkastenFile(): boolean {
  return (
    workspace.getConfiguration(APP_NAME).get('saveZettelkastenFile') || false
  );
}

export {
  getFilenameSpaceReplacement,
  getVisualizationBackground,
  getSaveZettelkastenFile,
};
