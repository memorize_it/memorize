import {
  CompletionItem,
  SnippetString,
  CompletionItemProvider,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { loadZettelkasten } from '@memorize/file-system';
import { getRootPath } from '../commands/vscode-utils';
import { getSaveZettelkastenFile } from '../config';

export const tagCompletionProvider: CompletionItemProvider = {
  async provideCompletionItems() {
    const rootPath = getRootPath();
    if (!rootPath) {
      return [];
    }
    const zettelkasten = await loadZettelkasten(
      rootPath,
      getSaveZettelkastenFile(),
    );
    return zettelkasten.meta.tags.map(
      (h): CompletionItem => {
        const idCompletion = new CompletionItem(`#${h}`);
        idCompletion.insertText = new SnippetString(h);
        return idCompletion;
      },
    );
  },
};
