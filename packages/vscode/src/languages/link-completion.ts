import {
  CompletionItem,
  SnippetString,
  CompletionItemProvider,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { loadZettelkasten } from '@memorize/file-system';
import { getRootPath } from '../commands/vscode-utils';
import { getSaveZettelkastenFile } from '../config';

export const linkCompletionProvider: CompletionItemProvider = {
  async provideCompletionItems() {
    const rootPath = getRootPath();
    if (!rootPath) {
      return [];
    }
    const zettelkasten = await loadZettelkasten(
      rootPath,
      getSaveZettelkastenFile(),
    );
    return zettelkasten.zettels.map(
      ({ id, title }): CompletionItem => {
        const idCompletion = new CompletionItem(`${id} - ${title}`);
        idCompletion.insertText = new SnippetString(id.toString());
        return idCompletion;
      },
    );
  },
};
