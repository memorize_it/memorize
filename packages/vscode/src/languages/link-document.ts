import {
  DocumentLinkProvider,
  DocumentLink,
  Uri,
  Range,
  TextLine,
  // eslint-disable-next-line import/no-unresolved
} from 'vscode';
import { IDRegexp } from '@memorize/core';
import { loadZettelkasten } from '@memorize/file-system';
import { ZettelInterface } from '@memorize/types';
import { getRootPath } from '../commands/vscode-utils';
import { getSaveZettelkastenFile } from '../config';

function buildLink(
  line: TextLine,
  lineIndex: number,
  {
    id,
    metadata: { directory, filenameWithExtension },
    title: tooltip,
  }: ZettelInterface,
): DocumentLink {
  const stringId = id.toString();
  const startCharacter = line.text.indexOf(stringId);
  const endCaracter = startCharacter + stringId.length;
  const range = new Range(lineIndex, startCharacter, lineIndex, endCaracter);
  const target = Uri.parse(`${directory}/${filenameWithExtension}`);

  return {
    range,
    target,
    tooltip,
  };
}

export const linkDocumentProvider: DocumentLinkProvider = {
  async provideDocumentLinks(document): Promise<DocumentLink[]> {
    const rootPath = getRootPath();
    if (!rootPath) {
      return [];
    }

    const links: DocumentLink[] = [];
    const zettelkasten = await loadZettelkasten(
      rootPath,
      getSaveZettelkastenFile(),
    );

    for (let lineIndex = 0; lineIndex < document.lineCount; lineIndex++) {
      const line = document.lineAt(lineIndex);
      const matches = line.text
        .match(IDRegexp)
        ?.map((id) => Number.parseInt(id));

      if (matches) {
        matches.forEach((id) => {
          const zettel = zettelkasten.zettels.find((z) => z.id === id);
          if (zettel) {
            links.push(buildLink(line, lineIndex, zettel));
          }
        });
      }
    }

    return links;
  },
};
