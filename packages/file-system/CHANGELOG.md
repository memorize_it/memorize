# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.9.1](https://gitlab.com/memorize_it/memorize/compare/v0.9.0...v0.9.1) (2021-04-18)

**Note:** Version bump only for package @memorize/file-system





# [0.9.0](https://gitlab.com/memorize_it/memorize/compare/v0.8.1...v0.9.0) (2021-04-18)


### Bug Fixes

* **file-system:** load filesWithErrors when loading a new zettelkasten ([be020ef](https://gitlab.com/memorize_it/memorize/commit/be020ef114d83c3eb359d97060f75feeb89c3a03))
* **file-system:** on generateZettelsForEmptyLinksInZettel complete only empty links ([9692d04](https://gitlab.com/memorize_it/memorize/commit/9692d04360a6b9b75a1c5f7ffa498056c43ea405))
* **file-system:** preserve existing zettel format when creating missing zettels ([83d9400](https://gitlab.com/memorize_it/memorize/commit/83d9400c7a72ec2e4feaf8391527e732c23e1456))


### Features

* **file-system:** extract selection to new zettel ([1200a89](https://gitlab.com/memorize_it/memorize/commit/1200a895e83ad79a25593548bf791c104a4523d5))
* **file-system:** user must specify if it wants to save zettelkasten file ([3e03ac3](https://gitlab.com/memorize_it/memorize/commit/3e03ac36b98b74b596892835f464c09702548ab8))





## [0.8.1](https://gitlab.com/memorize_it/memorize/compare/v0.8.0...v0.8.1) (2020-12-30)


### Bug Fixes

* **file-system:** handle short path cases ([e410bee](https://gitlab.com/memorize_it/memorize/commit/e410beef1a45833990d3629641d363d86961fd47))





# [0.8.0](https://gitlab.com/memorize_it/memorize/compare/v0.7.2...v0.8.0) (2020-12-27)


### Features

* **file-system:** when loading the zettelkasten log all the files with errors ([faae390](https://gitlab.com/memorize_it/memorize/commit/faae390736645e9c8b2aac7bc1f548e4c753646d))
* **file-system:** zettel id to path ([bc7c19c](https://gitlab.com/memorize_it/memorize/commit/bc7c19cc4268e19a018c179b8663da9972f8054f))





## [0.7.2](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.2) (2020-09-21)


### Features

* **file-system:** add zettel header to file ([cea86c0](https://gitlab.com/memorize_it/memorize/commit/cea86c0e0641555e512c79482e9ce4dcc4aa1d68))
* **file-system:** generate files for empty links ([d27a340](https://gitlab.com/memorize_it/memorize/commit/d27a340ab746ade2af9489de737b4b7c70c67202))
* **file-system:** load full zettelkasten if version doesn't match ([81a210d](https://gitlab.com/memorize_it/memorize/commit/81a210dffc9ff23c03be581a9cb7edfef549139d))
* **file-system:** retrieve zettel with content ([9e2c1b9](https://gitlab.com/memorize_it/memorize/commit/9e2c1b938e8c6dae8c536902dbe6eff3bfbd7bba))





## [0.7.1](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.1) (2020-09-21)


### Features

* **file-system:** add zettel header to file ([cea86c0](https://gitlab.com/memorize_it/memorize/commit/cea86c0e0641555e512c79482e9ce4dcc4aa1d68))
* **file-system:** generate files for empty links ([d27a340](https://gitlab.com/memorize_it/memorize/commit/d27a340ab746ade2af9489de737b4b7c70c67202))
* **file-system:** load full zettelkasten if version doesn't match ([81a210d](https://gitlab.com/memorize_it/memorize/commit/81a210dffc9ff23c03be581a9cb7edfef549139d))
* **file-system:** retrieve zettel with content ([9e2c1b9](https://gitlab.com/memorize_it/memorize/commit/9e2c1b938e8c6dae8c536902dbe6eff3bfbd7bba))





# [0.6.0](https://gitlab.com/memorize_it/memorize/compare/v0.5.0...v0.6.0) (2020-08-07)


### Features

* **file-system:** return fillepath on write ([8c80af3](https://gitlab.com/memorize_it/memorize/commit/8c80af37f64ba4b05f524b95d79b5afc6a263a8c))





# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)

**Note:** Version bump only for package @memorize/file-system





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **file-system:** addtags to metadata when loading zettelkasten ([33db0c6](https://gitlab.com/memorize_it/memorize/commit/33db0c62cc6a69d9fb0c310959ca14ac03e0fa4b))





## [0.3.1](https://gitlab.com/memorize_it/memorize/compare/v0.3.0...v0.3.1) (2020-02-08)


### Bug Fixes

* **types:** use @memorize/types as dependency to allow use of enums,maps... on runtime ([58c64b0](https://gitlab.com/memorize_it/memorize/commit/58c64b06160c105ac9073e82de8b4d087883fe68))





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)


### Bug Fixes

* **core:** move regeps from types to core. They are used on the runtime !!! XD ([c613128](https://gitlab.com/memorize_it/memorize/commit/c6131288b5c50d6962e8c59ee6f4df03a4d4c60d))


### Features

* **file-system:** add diagram type to zettelkasten visualization ([ca5e736](https://gitlab.com/memorize_it/memorize/commit/ca5e736a0ee61b587896c9c61aa9e74a8e260ca2))
* **file-system:** create svg file with zettelkasten visualization ([3a13c93](https://gitlab.com/memorize_it/memorize/commit/3a13c9349073d444eac024fcd221b19adf56730c))





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **file-system:** add basic file-system operations ([123c5f0](https://gitlab.com/memorize_it/memorize/commit/123c5f091c74352a7f80199abd185863021f3ef2))
* **file-system:** parse zettel from file and zettel to file ([e24a35d](https://gitlab.com/memorize_it/memorize/commit/e24a35df0b1160480021f1e0620b72bf8b85e476))
