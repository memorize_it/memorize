'use strict';
const baseConfig = require('../../jest.config');

module.exports = {
  ...baseConfig,
  setupFiles: ['jest-canvas-mock'],
};
