import { writeFile, basicPathToString } from './file-system';

describe('Testing File-System', () => {
  describe('When I want to write a file', () => {
    describe('And I provide a filename with no extension and the filename has no extension', () => {
      test('It should throw the fullpath', async () => {
        expect(
          writeFile({
            filename: 'myNewFile.md',
            directory: './new/path',
          }),
        ).rejects.toThrow();
      });
    });
  });

  describe('When I want to transform a basic path ', () => {
    describe('And I provide a directory', () => {
      describe('And the directory contains the filename', () => {
        describe('And the filename contains an extension', () => {
          test('It should throw an error', () => {
            expect(
              basicPathToString({
                directory: './new/path/newFile.md',
              }),
            ).toEqual('./new/path/newFile.md');
          });
        });

        describe('And the filename does not contain an extension', () => {
          test('It should throw an error', () => {
            expect(() =>
              basicPathToString({
                directory: './new/path/newFile',
              }),
            ).toThrow();
          });
        });
      });

      describe('And the directory does not contain a filename', () => {
        test('It should throw an error', () => {
          expect(() =>
            basicPathToString({
              directory: './new/path',
            }),
          ).toThrow();
        });

        describe('And it does contain an extension', () => {
          test('It should throw an error', () => {
            expect(() =>
              basicPathToString({
                directory: './new/path',
                extension: '.md',
              }),
            ).toThrow();
          });
        });
      });

      describe('And I provide a filename', () => {
        describe('And I do not provide an extension', () => {
          describe('And the filename contains an extension', () => {
            test('It should return the fullpath', () => {
              expect(
                basicPathToString({
                  filename: 'myNewFile.md',
                  directory: './new/path',
                }),
              ).toEqual('./new/path/myNewFile.md');
            });

            describe('And the path is short', () => {
              test('It should return the fullpath', () => {
                expect(
                  basicPathToString({
                    filename: 'myNewFile.md',
                    directory: './new',
                  }),
                ).toEqual('./new/myNewFile.md');
              });
            });

            describe('And the path is too short', () => {
              test('It should return the fullpath', () => {
                expect(
                  basicPathToString({
                    filename: 'myNewFile.md',
                    directory: '.',
                  }),
                ).toEqual('./myNewFile.md');
              });
            });
          });

          describe('And the filename has no extension', () => {
            test('It should throw an error', () => {
              expect(() =>
                basicPathToString({
                  filename: 'myNewFile',
                  directory: './new/path',
                }),
              ).toThrow();
            });
          });
        });

        describe('And I provide an extension', () => {
          describe('And I provide an extension with a dot', () => {
            test('It should return the fullpath', () => {
              expect(
                basicPathToString({
                  filename: 'myNewFile',
                  extension: '.md',
                  directory: './new/path',
                }),
              ).toEqual('./new/path/myNewFile.md');
            });
          });

          describe('And I provide an extension without a dot', () => {
            test('It should return the fullpath', () => {
              expect(
                basicPathToString({
                  filename: 'myNewFile',
                  extension: 'md',
                  directory: './new/path',
                }),
              ).toEqual('./new/path/myNewFile.md');
            });
          });

          describe('And I provide an extension different from the filename', () => {
            test('It should return the fullpath', () => {
              expect(
                basicPathToString({
                  filename: 'myNewFile.json',
                  extension: '.md',
                  directory: './new/path',
                }),
              ).toEqual('./new/path/myNewFile.md');
            });
          });
        });
      });
    });
  });
});
