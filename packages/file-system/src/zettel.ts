import {
  findZettel,
  getRelatedZettels,
  id,
  IDRegexp,
  sanitizeText,
} from '@memorize/core';
import {
  ZettelBodyInterface,
  ZettelId,
  ZettelInterface,
  ZettelWithContentInterface,
} from '@memorize/types';
import { hasOwnArrayProperty, hasOwnProperty } from '@memorize/utils';
import { dump, load } from 'js-yaml';
import { basename, dirname } from 'path';
import { parse as parseMd } from 'remark';
import parse from 'remark-parse';
import stringify from 'remark-stringify';
import unified from 'unified';
// eslint-disable-next-line import/no-unresolved
import { Node } from 'unist';
import visit from 'unist-util-visit';
import { readFile, writeBeggingOfFile, writeFile } from './file-system';
import linkCreator from './link-creator';
import { loadZettelkasten } from './zettelkasten';

const separator = '---';
export const TagRegexp = /(\#[a-zA-Z0-9_\-]+)/gm;

export function splitZettel(content: string): { meta: string; body: string } {
  const [, meta, body] = content.split(separator);
  return { meta, body };
}

function searchInString(regex: RegExp, str: string): string[] {
  const result: string[] = [];
  let m: RegExpExecArray | null;

  while ((m = regex.exec(str)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    const match = m.pop();

    if (match) {
      result.push(match);
    }
  }
  return result;
}

export function contentToZettelBody(content: string): ZettelBodyInterface {
  const { meta, body } = splitZettel(content);

  let title = '';

  try {
    visit<Node & { children: (Node & { value: string })[] }>(
      parseMd(body),
      'heading',
      ({ children }) => {
        if (!title) {
          title = children[0].value;
        }
      },
    );
  } catch {
    title = '';
  }

  if (!meta) {
    throw new Error(
      `file-system(contentToZettelBody): unable to parse zettel content, ensure all markdown files respect the format`,
    );
  }

  const metadataInformation = load(meta);
  if (
    !metadataInformation ||
    typeof metadataInformation !== 'object' ||
    !hasOwnProperty(metadataInformation, 'id') ||
    typeof metadataInformation.id !== 'number'
  ) {
    throw new Error(
      `file-system(contentToZettelBody): unable to parse zettel content, yaml doesn't respect the format`,
    );
  }

  const references: ZettelId[] = cleanMetadataInformation<ZettelId>(
    metadataInformation,
    'references',
    body,
    () => searchInString(IDRegexp, body).map((id) => Number.parseInt(id)),
  );

  const tags: string[] = cleanMetadataInformation<string>(
    metadataInformation,
    'tags',
    body,
    () => searchInString(TagRegexp, body).map((t) => t.replace('#', '')),
  );

  return {
    id: metadataInformation.id,
    references,
    tags,
    title,
  };
}

function cleanMetadataInformation<T>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  metadataInformation: object,
  property: string,
  body: string,
  concatFunction: () => T[],
): T[] {
  if (!hasOwnArrayProperty(metadataInformation, property)) {
    return [];
  }
  const array = metadataInformation[property] as T[];
  return body ? array.concat(concatFunction()) : array;
}

export async function generateZettelsForEmptyLinksInZettel(
  directory: string,
  filenameSpaceReplacement?: string,
): Promise<string> {
  const { meta, body } = splitZettel(await readFile({ directory }));
  const zettel = await parseZettelOnFullPath(directory);
  const newContent = await unified()
    .use(parse)
    .use(linkCreator, {
      parentZettelId: zettel.id,
      directory: zettel.metadata.directory,
      filenameSpaceReplacement,
    })
    .use(stringify, {
      bullet: '-',
      listItemIndent: 'one',
    })
    .process(body);

  return updateZettelContent(zettel, meta.trim(), newContent.toString());
}

export function getValidId(
  usedIds: ZettelId[],
  zettelId: ZettelId,
): { zettelId: ZettelId; usedIds: ZettelId[] } {
  if (!usedIds.includes(zettelId)) {
    usedIds.push(zettelId);
    return { zettelId, usedIds };
  }

  return getValidId(usedIds, zettelId + 1);
}

export async function parseZettelOnFullPath(
  directory: string,
): Promise<ZettelInterface> {
  try {
    const zettelFile = await readFile({
      directory,
    });

    return {
      ...contentToZettelBody(zettelFile),
      metadata: {
        directory: dirname(directory),
        filenameWithExtension: basename(directory),
      },
    };
  } catch {
    throw new Error(
      `file-system(parseZettelOnFullPath): unable to parse zettel in path "${directory}"`,
    );
  }
}

export async function getZettelWithContent(
  directory: string,
  id: ZettelId,
  storeZettelkastenFile: boolean,
): Promise<ZettelWithContentInterface> {
  const zettelkasten = await loadZettelkasten(directory, storeZettelkastenFile);
  const zettel = findZettel(zettelkasten, id);
  try {
    return {
      ...zettel,
      content: await readFile({
        directory: zettel.metadata.directory,
        filename: zettel.metadata.filenameWithExtension,
      }),
      relatedZettels: getRelatedZettels(zettelkasten, id),
    };
  } catch {
    throw new Error(
      `file-system(getZettelWithContent): unable to parse zettel"`,
    );
  }
}

export function zettelToContent(
  { id, tags, references, title }: ZettelInterface,
  content?: string,
): string {
  return frontmatterAndContent(
    dumpZettelHeader({
      id,
      tags,
      references,
    }),
    content ? content : `# ${title}`,
  );
}

export function frontmatterAndContent(
  frontmatter: string,
  content?: string,
): string {
  const base = [separator, frontmatter, separator];

  if (content) {
    if (!content.startsWith('\n')) {
      base.push('');
    }

    base.push(content.trim());
  }

  base.push('');
  return base.join('\n');
}

export async function writeZettel(
  zettel: ZettelInterface,
  content?: string,
): Promise<string> {
  const {
    metadata: { directory, filenameWithExtension },
  } = zettel;
  return writeFile({
    directory: directory,
    filename: filenameWithExtension,
    content: zettelToContent(zettel, content),
  });
}

export async function updateZettelContent(
  zettel: ZettelInterface,
  frontmatter: string,
  newContent: string,
): Promise<string> {
  const {
    metadata: { directory, filenameWithExtension },
  } = zettel;
  return writeFile({
    directory: directory,
    filename: filenameWithExtension,
    content: frontmatterAndContent(frontmatter, newContent),
  });
}

function dumpZettelHeader(header: {
  id: ZettelId;
  tags: string[];
  references: ZettelId[];
}) {
  return dump(
    { ...header },
    {
      indent: 1,
    },
  ).trimEnd();
}

export function emptyContent(): string {
  return [
    separator,
    dumpZettelHeader({
      id: id(new Date()),
      tags: [],
      references: [],
    }),
    separator,
    '',
  ].join('\n');
}

export async function addZettelHeaderToFile(path: string): Promise<void> {
  return writeBeggingOfFile(path, emptyContent());
}

export async function zettelIdToPath(
  zettelkastenDirectory: string,
  id: ZettelId,
  saveZettelkastenFile: boolean,
): Promise<string> {
  const {
    metadata: { directory, filenameWithExtension },
  } = findZettel(
    await loadZettelkasten(zettelkastenDirectory, saveZettelkastenFile),
    id,
  );
  return [directory, filenameWithExtension].join('/');
}

export async function extractNewZettelFromSelection(
  selection: string,
  parentZettelFullpath: string,
  filenameSpaceReplacement?: string,
): Promise<string> {
  const parentZettel = await parseZettelOnFullPath(parentZettelFullpath);
  let title: string | undefined;

  visit<Node & { children: (Node & { value: string })[] }>(
    parseMd(selection),
    'heading',
    ({ children }) => {
      if (!title) {
        title = children[0].value;
      }
    },
  );

  if (!title) {
    throw new Error(
      `file-system(extractNewZettelFromSelection): selection doesn't contain a valid heading`,
    );
  }

  const zettelId = id(new Date());
  await writeZettel(
    {
      id: zettelId,
      metadata: {
        filenameWithExtension: `${sanitizeText(
          title,
          filenameSpaceReplacement,
        )}.md`,
        directory: parentZettel.metadata.directory,
      },
      references: [parentZettel.id],
      tags: [],
      title,
    },
    selection,
  );

  return `[${title}](${zettelId})`;
}
