import {
  zettelToContent,
  contentToZettelBody,
  generateZettelsForEmptyLinksInZettel,
  extractNewZettelFromSelection,
} from './zettel';
import { ZettelInterface, ZettelBodyInterface } from '@memorize/types';
import * as FileSystem from './file-system';

jest.mock('@memorize/core', () => ({
  ...jest.requireActual('@memorize/core'),
  id: jest.fn().mockReturnValue(2),
}));

describe('Testing Zettel', () => {
  const awesomeZettelBody: ZettelBodyInterface = {
    id: 20200120160015,
    references: [],
    tags: ['test', 'awesome'],
    title: 'My awesome title',
  };

  const awesomeZettel: ZettelInterface = {
    ...awesomeZettelBody,
    metadata: {
      directory: '~/zettelkasten/',
      filenameWithExtension: 'test.md',
    },
  };

  const awesomeZettelBasicContent = [
    '---',
    'id: 20200120160015',
    'tags:',
    ' - test',
    ' - awesome',
    'references: []',
    '---',
    '',
    '# My awesome title',
    '',
  ];

  describe('When I want to convert a zettel into markdown content', () => {
    const awesomeZettelContent = awesomeZettelBasicContent.join('\n');

    describe('And when no content is provided', () => {
      test('It should return a string with yaml metadata and header', () => {
        expect(zettelToContent(awesomeZettel)).toEqual(awesomeZettelContent);
      });
    });

    describe('And when content is provided', () => {
      test('It should return a string with yaml metadata and the content after the separator', () => {
        const newContent = '# New Content';
        const result = [
          '---',
          'id: 20200120160015',
          'tags:',
          ' - test',
          ' - awesome',
          'references: []',
          '---',
          '',
          newContent,
          '',
        ].join('\n');

        expect(zettelToContent(awesomeZettel, newContent)).toEqual(result);
      });
    });
  });

  describe('When I want to convert a markdown content into a zettel', () => {
    test('It should parse metadata and content', () => {
      const markdown = [
        ...awesomeZettelBasicContent,
        '## My other awesome title',
        '',
      ].join('\n');

      expect(contentToZettelBody(markdown)).toEqual(awesomeZettelBody);
    });

    test('It should parse metadata and content and search for tags', () => {
      const zettel: ZettelBodyInterface = {
        ...awesomeZettelBody,
        tags: [...awesomeZettel.tags, 'new_tag'],
      };
      const markdown = [
        ...awesomeZettelBasicContent,
        'This is a sentence with a #new_tag',
        '',
      ].join('\n');

      expect(contentToZettelBody(markdown)).toEqual(zettel);
    });

    test('It should parse metadata and content and search for references', () => {
      const zettel: ZettelBodyInterface = {
        ...awesomeZettelBody,
        references: [20200120160016, 20200120160017],
      };

      const markdown = [
        '---',
        'id: 20200120160015',
        'tags:',
        ' - test',
        ' - awesome',
        'references:',
        ' - 20200120160016',
        '---',
        '# My awesome title',
        '',
        'This is a sentence with a reference to 20200120160017',
      ].join('\n');

      expect(contentToZettelBody(markdown)).toEqual(zettel);
    });

    describe('And when content is empty', () => {
      test('It should parse metadata and search for references', () => {
        const zettel: ZettelBodyInterface = {
          ...awesomeZettelBody,
          title: '',
          references: [20200120160016],
        };

        const markdown = [
          '---',
          'id: 20200120160015',
          'tags:',
          ' - test',
          ' - awesome',
          'references:',
          ' - 20200120160016',
          '---',
        ].join('\n');

        expect(contentToZettelBody(markdown)).toEqual(zettel);
      });
    });
  });

  describe('When I want to generate zettels for empty links', () => {
    let writeFileSpy: jest.SpyInstance;
    let readFileSpy: jest.SpyInstance;
    const parentId = 1;
    const parentPath = '/parent.md';

    beforeEach(() => {
      readFileSpy = jest.spyOn(FileSystem, 'readFile');
      writeFileSpy = jest.spyOn(FileSystem, 'writeFile');
      writeFileSpy.mockResolvedValue('/test_2.md');
    });

    describe('When zettel file has empty links', () => {
      const content = [
        '---',
        `id: ${parentId}`,
        'tags:',
        '  - tag',
        'references: []',
        '---',
        '',
        '# Test 1',
        '',
        '- [Test 2]()',
        '- [Test 3](LINK)',
        '',
      ].join('\n');

      beforeEach(() => {
        readFileSpy.mockImplementation(({ directory }) => {
          if (directory === parentPath) {
            return Promise.resolve(content);
          }

          return Promise.reject();
        });
      });

      test('I want to write a file', async () => {
        const newId = 2;
        const childContent = [
          '---',
          `id: ${newId}`,
          'tags: []',
          'references:',
          ` - ${parentId}`,
          '---',
          '',
          '# Test 2',
          '',
        ].join('\n');

        const newContent = [
          '---',
          `id: ${parentId}`,
          'tags:',
          '  - tag',
          'references: []',
          '---',
          '',
          '# Test 1',
          '',
          `- [Test 2](${newId})`,
          '- [Test 3](LINK)',
          '',
        ].join('\n');

        await generateZettelsForEmptyLinksInZettel(parentPath);

        expect(writeFileSpy).toHaveBeenCalledTimes(2);
        expect(writeFileSpy).toHaveBeenNthCalledWith(1, {
          directory: '/',
          filename: 'test_2.md',
          content: childContent,
        });
        expect(writeFileSpy).toHaveBeenNthCalledWith(2, {
          directory: '/',
          filename: 'parent.md',
          content: newContent,
        });
      });
    });

    describe('When zettel file has not empty links', () => {
      const content = [
        '---',
        `id: '${parentId}'`,
        'tags:',
        '  - tag',
        'references: []',
        '---',
        '',
        '# Test 1',
        '',
        '[test 2](link)',
        '',
      ].join('\n');

      beforeEach(() => {
        readFileSpy.mockImplementation(({ directory }) => {
          if (directory === parentPath) {
            return Promise.resolve(content);
          }

          return Promise.reject();
        });
      });

      test('I do not want to write a file', async () => {
        expect(writeFileSpy).not.toHaveBeenCalled();
      });
    });
  });

  describe.only('When I want to extract zettel from selection', () => {
    let writeFileSpy: jest.SpyInstance;
    let readFileSpy: jest.SpyInstance;

    const parentId = 1;
    const parentPath = '/parent.md';
    const parentContent = [
      '---',
      `id: ${parentId}`,
      'tags:',
      '  - tag',
      'references: []',
      '---',
      '',
      '# Parent content does not matter as selection received',
      '',
    ].join('\n');

    beforeEach(() => {
      readFileSpy = jest.spyOn(FileSystem, 'readFile');
      writeFileSpy = jest.spyOn(FileSystem, 'writeFile');
      readFileSpy.mockImplementation(({ directory }) => {
        if (directory === parentPath) {
          return Promise.resolve(parentContent);
        }

        return Promise.reject();
      });

      writeFileSpy.mockResolvedValue('/text_to_extract.md');
    });

    describe('And selection contains heading', () => {
      test('I want to write a file', async () => {
        const newId = 2;

        const selectionContent = [
          '# Text to extract',
          '',
          '- New List',
          '- AnotherList',
        ];
        const selection = selectionContent.join('\n');
        const childContent = [
          '---',
          `id: ${newId}`,
          'tags: []',
          'references:',
          ` - ${parentId}`,
          '---',
          '',
          ...selectionContent,
          '',
        ].join('\n');

        const result = await extractNewZettelFromSelection(
          selection,
          parentPath,
        );

        expect(writeFileSpy).toHaveBeenCalledTimes(1);
        expect(writeFileSpy).toHaveBeenNthCalledWith(1, {
          directory: '/',
          filename: 'text_to_extract.md',
          content: childContent,
        });
        expect(result).toEqual(`[Text to extract](${newId})`);
      });
    });

    describe('And selection does not contain heading', () => {
      test('I want to throw an error', async () => {
        const selectionContent = ['- New List', '- AnotherList'];
        const selection = selectionContent.join('\n');
        await expect(
          extractNewZettelFromSelection(selection, parentPath),
        ).rejects.toThrow();
        expect(writeFileSpy).not.toHaveBeenCalled();
      });
    });
  });
});
