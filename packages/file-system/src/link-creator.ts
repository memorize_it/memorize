import { id, sanitizeText } from '@memorize/core';
import { ZettelId } from '@memorize/types';
import { removeFileAsync } from '@memorize/utils';
import { Plugin } from 'unified';
// eslint-disable-next-line import/no-unresolved
import { Node } from 'unist';
import visit from 'unist-util-visit';
import { getValidId, writeZettel } from './zettel';

interface LinkCreatorOptionsInterface {
  parentZettelId: ZettelId;
  directory: string;
  filenameSpaceReplacement?: string;
}

const linkCreator: Plugin<[LinkCreatorOptionsInterface]> = ({
  parentZettelId,
  directory,
  filenameSpaceReplacement,
}: LinkCreatorOptionsInterface) => {
  return async (tree) => {
    const createdZettelPaths: string[] = [];
    const usedIds: ZettelId[] = [];
    try {
      const linkNodes: Node[] = [];
      visit(tree, 'link', (link) => linkNodes.push(link));

      await Promise.all(
        linkNodes.map(async (z: any) => {
          if (z.url) {
            return z;
          }

          const title: string = z.children.find((c: Node) => c.type === 'text')
            .value;
          const { zettelId } = getValidId(usedIds, id(new Date()));

          const zettelPath = await writeZettel({
            id: zettelId,
            metadata: {
              filenameWithExtension: `${sanitizeText(
                title,
                filenameSpaceReplacement,
              )}.md`,
              directory: directory,
            },
            references: [parentZettelId],
            tags: [],
            title,
          });

          z.url = zettelId.toString();
          createdZettelPaths.push(zettelPath);
        }),
      );

      return tree;
    } catch (e) {
      await Promise.all(createdZettelPaths.map(removeFileAsync));
      throw e;
    }
  };
};

export default linkCreator;
