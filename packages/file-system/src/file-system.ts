import {
  readFileAsync,
  readdirAsync,
  statAsync,
  writeFileAsync,
  writeAsync,
  openAsync,
  closeAsync,
} from '@memorize/utils';
import { basename, dirname, extname } from 'path';
import { Dirent } from 'fs';

export interface BasicPathInterface {
  directory: string;
  filename?: string;
  extension?: string;
}

export interface PathInterface extends BasicPathInterface {
  filename: string;
}

export interface NewFileInterface extends PathInterface {
  content?: string | unknown;
}

export interface NewFileAsJsonInterface extends NewFileInterface {
  extension: 'json';
}

export async function writeFile({
  filename,
  directory,
  extension,
  content,
}: NewFileInterface): Promise<string> {
  const fullpath: string = basicPathToString({
    filename,
    directory,
    extension,
  });

  await writeFileAsync(
    fullpath,
    content
      ? typeof content === 'string'
        ? content
        : JSON.stringify(content)
      : '',
  );
  return fullpath;
}

export async function writeBeggingOfFile(
  path: string,
  newText: string,
): Promise<void> {
  const data = await readFileAsync(path);
  const fd = await openAsync(path, 'w+');
  const buffer = Buffer.from(newText);

  await writeAsync(fd, buffer, 0, buffer.length, 0);
  await writeAsync(fd, data, 0, data.length, buffer.length);

  return closeAsync(fd);
}

export function basicPathToString({
  filename,
  directory,
  extension,
}: BasicPathInterface): string {
  if (!filename && extension) {
    throw new Error(
      'file-system(basicPathToString) : you provide an extension but you do not provide a filename',
    );
  }

  if (!filename) {
    filename = basename(directory);
    directory = dirname(directory);
  }

  if (!filename) {
    throw new Error(
      `file-system(basicPathToString) : provided directory has no filename - "${directory}" `,
    );
  }

  if (!extension) {
    extension = extname(filename);
    filename = basename(filename, extension);
  } else {
    const filenameExtension = extname(filename);
    if (filenameExtension) {
      filename = basename(filename, filenameExtension);
    }
  }

  if (!extension) {
    throw new Error(
      `file-system(basicPathToString) : provided filename has no extension - "${filename}" `,
    );
  }

  extension = extension.includes('.') ? extension : `.${extension}`;

  return `${directory}/${filename}${extension}`;
}

export async function readFile(path: BasicPathInterface): Promise<string> {
  const fileContent = await readFileAsync(basicPathToString(path));
  return fileContent.toString('utf8');
}

export async function readFromJson<T>(path: BasicPathInterface): Promise<T> {
  const correctExtension = 'json';
  const pathString = basicPathToString(path);

  const extension = extname(pathString);

  if (extension != correctExtension) {
    throw new Error(
      `file-system(readFromJson) : provided path is not a JSON file - "${pathString}" `,
    );
  }

  return JSON.parse(await readFile(path));
}

export async function writeAsJson({
  filename,
  directory,
  content,
}: NewFileAsJsonInterface): Promise<string> {
  const filenameExtension = extname(filename);
  if (filenameExtension) {
    filename = basename(filename, filenameExtension);
  }

  return writeFile({
    content: JSON.stringify(content, null, '\t'),
    extension: 'json',
    filename,
    directory,
  });
}

export async function searchFilesInDirectoryWithExtension(
  path: string,
  extension: string,
  updatedAfterMs?: number,
): Promise<string[]> {
  let dir: Dirent[] | undefined;
  try {
    dir = await readdirAsync(path, {
      withFileTypes: true,
    });
  } catch {
    throw new Error(
      `file-system(searchFilesInDirectory): unable to read directory in path "${path}"`,
    );
  }

  const mdFiles = await Promise.all(
    dir
      .filter(
        (dirent): boolean =>
          dirent.isFile() && extname(dirent.name) === extension,
      )
      .map(({ name }): string => `${path}/${name}`),
  );

  if (!updatedAfterMs) {
    return mdFiles;
  }

  const updatedFiles = await Promise.all(
    mdFiles.map(
      async (path): Promise<string | undefined> => {
        const { mtimeMs } = await statAsync(path);
        return mtimeMs > updatedAfterMs ? path : undefined;
      },
    ),
  );

  return updatedFiles.filter(Boolean) as string[];
}
