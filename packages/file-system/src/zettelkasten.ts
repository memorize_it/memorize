import {
  ZettelInterface,
  ZettelkastenCommonMetaInterface,
  ZettelkastenInterface,
  ZettelkastenVisualizationType,
} from '@memorize/types';
import { getZettelkastenVisualizationSvg } from '@memorize/visualization';
import {
  readFromJson,
  writeAsJson,
  writeFile,
  searchFilesInDirectoryWithExtension,
} from './file-system';
import { parseZettelOnFullPath } from './zettel';
import { version } from './version';
import { createZettelkasten } from '@memorize/core';

const zettelkastenFilename = '.zettelkasten.json';

export interface ParseZettelkastenDirectoryInterface {
  zettels: ZettelInterface[];
  filesWithError: string[];
}

export interface FileSystemZettelkastenInterface extends ZettelkastenInterface {
  meta: { filesWithError: string[] } & ZettelkastenCommonMetaInterface;
}

export async function parseZettelkastenDirectory(
  directory: string,
  lastUpdateTime?: number,
): Promise<ParseZettelkastenDirectoryInterface> {
  const zettels = await searchFilesInDirectoryWithExtension(
    directory,
    '.md',
    lastUpdateTime,
  );

  const result: Array<ZettelInterface | string> = await Promise.all(
    zettels.map(
      async (zettelPath): Promise<ZettelInterface | string> => {
        try {
          const zettel = await parseZettelOnFullPath(zettelPath);
          return zettel;
        } catch {
          return zettelPath;
        }
      },
    ),
  );

  return result.reduce(
    (c: ParseZettelkastenDirectoryInterface, pathOrZettel) => {
      if (typeof pathOrZettel === 'string') {
        c.filesWithError.push(pathOrZettel);
      } else {
        c.zettels.push(pathOrZettel);
      }
      return c;
    },
    { zettels: [], filesWithError: [] },
  );
}

export async function loadZettelkasten(
  directory: string,
  saveZettelkastenFile: boolean,
): Promise<FileSystemZettelkastenInterface> {
  let zettelkasten: FileSystemZettelkastenInterface;

  const now = Date.now();

  try {
    zettelkasten = await readFromJson({
      directory,
      filename: zettelkastenFilename,
    });
    if (!zettelkasten.meta.version || zettelkasten.meta.version !== version) {
      throw new Error(
        version
          ? `file-system(loadZettelkasten): outdated zettelkasten version (${zettelkasten.meta.version}), current version (${version})`
          : 'file-system(loadZettelkasten): no zettelkasten version specified',
      );
    }

    if (zettelkasten.meta.version !== version) {
      throw new Error(
        'zettelkasten(loadZettelkasten): new version detected, reload full zettelkasten',
      );
    }

    const existingZettelIds = zettelkasten.zettels.map(({ id }) => id);
    const {
      zettels: updatedZettels,
      filesWithError,
    } = await parseZettelkastenDirectory(
      directory,
      zettelkasten.meta.updatedAt,
    );
    let tags = zettelkasten.meta.tags;

    updatedZettels.forEach((updatedZettel) => {
      const updatedZettelId = updatedZettel.id;
      const existingZettelId = existingZettelIds.findIndex(
        (id) => id === updatedZettelId,
      );

      if (existingZettelId < 0) {
        return zettelkasten.zettels.push(updatedZettel);
      }

      zettelkasten.zettels[existingZettelId] = updatedZettel;
      tags = [
        ...tags,
        ...updatedZettel.tags.filter((h) => tags.indexOf(h) < 0),
      ];
    });

    zettelkasten.meta.tags = tags;
    zettelkasten.meta.filesWithError = filesWithError;
    zettelkasten.meta.updatedAt = now;
  } catch {
    const zettelParse = await parseZettelkastenDirectory(directory, undefined);
    const plainZettelkasten = createZettelkasten({
      ...zettelParse,
      updatedAt: now,
    });
    zettelkasten = {
      ...plainZettelkasten,
      meta: {
        ...plainZettelkasten.meta,
        filesWithError: zettelParse.filesWithError,
      },
    };
  }

  if (saveZettelkastenFile) {
    await writeAsJson({
      directory,
      content: zettelkasten,
      extension: 'json',
      filename: zettelkastenFilename,
    });
  }

  return zettelkasten;
}

export async function createZettelkastenSvg(
  directory: string,
  type: ZettelkastenVisualizationType,
  saveZettelkastenFile: boolean,
): Promise<void> {
  const zettelkasten = await loadZettelkasten(directory, saveZettelkastenFile);
  writeFile({
    filename: 'zettelkasten',
    extension: 'svg',
    directory,
    content: await getZettelkastenVisualizationSvg(zettelkasten, type),
  });

  return;
}
