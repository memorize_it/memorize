import { ZettelkastenVisualizationType } from '@memorize/types';

export type TStringAnswer<T extends string> = T | string | undefined;
export type TStringArrayAnswer = string[];

export interface NameAnswer {
  name: TStringAnswer<string>;
}

export interface PathAnswer {
  path: TStringAnswer<string>;
}

export interface ZettelDetailsAnswer {
  filename: TStringAnswer<string>;
  tags: TStringArrayAnswer;
}

export interface ZettelTagAnswer {
  tag: TStringAnswer<string>;
}

export interface DiagramTypeAnswer {
  type: TStringAnswer<ZettelkastenVisualizationType>;
}
