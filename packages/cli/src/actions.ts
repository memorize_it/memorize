import { prompt } from 'inquirer';
import {
  pathQuestions,
  zettelNameQuestions,
  zettelDetailsQuestions,
  diagramTypeQuestions,
  validateTagQuestions,
} from './questions';
import { id, similarTags } from '@memorize/core';
import {
  PathAnswer,
  NameAnswer,
  ZettelDetailsAnswer,
  DiagramTypeAnswer,
  ZettelTagAnswer,
} from './types/actions';
import { addZettelAnswersToZettel } from './answers.mapper';
import {
  writeZettel,
  loadZettelkasten,
  createZettelkastenSvg,
} from '@memorize/file-system';
import chalk from 'chalk';
import { ZettelkastenVisualizationType } from '@memorize/types';
import { program } from 'commander';

export function generateId(): void {
  const date = new Date();
  console.log(chalk.green('The current id is: '));
  console.log(id(date));
}

export async function addZettel(): Promise<void> {
  const pathAnswer = await prompt<PathAnswer>(pathQuestions());
  const { path } = pathAnswer;
  if (!path) {
    throw new Error('actions(addZettel): path is required');
  }

  const nameAnswer = await prompt<NameAnswer>(zettelNameQuestions);
  if (!nameAnswer.name) {
    throw new Error('actions(addZettel): name is required');
  }

  const zettelDetailsAnswer = await prompt<ZettelDetailsAnswer>(
    zettelDetailsQuestions(nameAnswer.name),
  );

  const { tags } = zettelDetailsAnswer;
  if (tags.length) {
    const existingTags = similarTags(
      tags,
      await loadZettelkasten(path, program.opts().save),
    );

    const consolidatedTags: string[] = [];
    for (let index = 0; index < tags.length; index++) {
      const t = tags[index];

      const eTag = existingTags[t];
      if (!eTag.length) {
        consolidatedTags[index] = t;
      }

      const { tag } = await prompt<ZettelTagAnswer>(
        validateTagQuestions(t, eTag),
      );

      if (!tag) {
        throw new Error(
          'actions(addZettel): when parsing similar tags an exisitng tag must be selected',
        );
      }
      consolidatedTags[index] = tag;
    }

    zettelDetailsAnswer.tags = consolidatedTags;
  }

  const zettel = addZettelAnswersToZettel(
    pathAnswer,
    nameAnswer,
    zettelDetailsAnswer,
  );

  await writeZettel(zettel);

  console.log(chalk.green('✔ The zettel is created !'));
}

export async function updateZettelkasten(): Promise<void> {
  const save = program.opts().save;
  if (!save) {
    console.log(
      chalk.red('❌ Updating the zettelkasten requires save (-s) option !'),
    );
    return;
  }

  const { path } = await prompt<PathAnswer>(pathQuestions());
  if (!path) {
    throw new Error('actions(updateZettelkasten): path is required');
  }

  await loadZettelkasten(path, save);

  console.log(chalk.green('✔ The zettelkasten is ready!'));
}

export async function generatePrintZettelkasten(): Promise<void> {
  const { path } = await prompt<PathAnswer>(pathQuestions());
  if (!path) {
    throw new Error('actions(generatePrintZettelkasten): path is required');
  }

  const { type } = await prompt<DiagramTypeAnswer>(diagramTypeQuestions());
  if (!type) {
    throw new Error('actions(generatePrintZettelkasten): type is required');
  }

  await createZettelkastenSvg(
    path,
    type as ZettelkastenVisualizationType,
    program.opts().save,
  );

  console.log(chalk.green('✔ Svg file generated !'));
}
