import { ZettelInterface } from '@memorize/types';
import { PathAnswer, NameAnswer, ZettelDetailsAnswer } from './types/actions';
import { id } from '@memorize/core';

export function addZettelAnswersToZettel(
  { path }: PathAnswer,
  { name }: NameAnswer,
  { filename, tags }: ZettelDetailsAnswer,
): ZettelInterface {
  if (!path) {
    throw new Error(
      `answers.mapper(addZettelAnswersToZettel): path must be specified`,
    );
  }

  if (!name) {
    throw new Error(
      `answers.mapper(addZettelAnswersToZettel): name must be specified`,
    );
  }

  if (!filename) {
    throw new Error(
      `answers.mapper(addZettelAnswersToZettel): filename must be specified`,
    );
  }

  if (!tags) {
    throw new Error(
      `answers.mapper(addZettelAnswersToZettel): tags must be specified or empty`,
    );
  }

  return {
    id: id(new Date()),
    title: name,
    references: [],
    tags,
    metadata: {
      directory: path,
      filenameWithExtension: `${filename}.md`,
    },
  };
}
