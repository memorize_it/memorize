#!/usr/bin/env node
import program from 'commander';
import chalk from 'chalk';
import figlet from 'figlet';
import { clear } from 'console';
import {
  generateId,
  addZettel,
  generatePrintZettelkasten,
  updateZettelkasten,
} from './actions';
import { version } from './version';

clear();
console.log(
  chalk.red(figlet.textSync('memorize-cli', { horizontalLayout: 'full' })),
);

program.version(version).description('Memorize CLI');

program.option('-s, --save', 'save zettelkaten file').parse();

program.command('id').description('generate current id').action(generateId);

program
  .command('addZettel')
  .alias('a')
  .description('Add zettel')
  .action(addZettel);

program
  .command('initialize')
  .alias('init')
  .alias('i')
  .alias('update')
  .alias('u')
  .description('Initialize/update zettelkasten')
  .action(updateZettelkasten);

program
  .command('print')
  .alias('p')
  .description('Generate zettelkasten svg')
  .action(generatePrintZettelkasten);

program.parse(process.argv);
