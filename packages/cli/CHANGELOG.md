# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.9.1](https://gitlab.com/memorize_it/memorize/compare/v0.9.0...v0.9.1) (2021-04-18)

**Note:** Version bump only for package @memorize/cli





# [0.9.0](https://gitlab.com/memorize_it/memorize/compare/v0.8.1...v0.9.0) (2021-04-18)


### Features

* **cli:** read -s option to save or not zettelkasten file ([985c63b](https://gitlab.com/memorize_it/memorize/commit/985c63b14dd88e0327863c5ab8dc2942f5f110e2))





## [0.8.1](https://gitlab.com/memorize_it/memorize/compare/v0.8.0...v0.8.1) (2020-12-30)

**Note:** Version bump only for package @memorize/cli





# [0.8.0](https://gitlab.com/memorize_it/memorize/compare/v0.7.2...v0.8.0) (2020-12-27)

**Note:** Version bump only for package @memorize/cli





## [0.7.2](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.2) (2020-09-21)


### Bug Fixes

* **cli:** add extension to file when creating new zettel ([f2b78af](https://gitlab.com/memorize_it/memorize/commit/f2b78af0c77b84e24238e6ab894ca48501d1d814))


### Features

* **cli:** dynamic version update ([70c5090](https://gitlab.com/memorize_it/memorize/commit/70c5090730613e741772e377ae4d628da147558d))





## [0.7.1](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.1) (2020-09-21)


### Bug Fixes

* **cli:** add extension to file when creating new zettel ([f2b78af](https://gitlab.com/memorize_it/memorize/commit/f2b78af0c77b84e24238e6ab894ca48501d1d814))


### Features

* **cli:** dynamic version update ([70c5090](https://gitlab.com/memorize_it/memorize/commit/70c5090730613e741772e377ae4d628da147558d))





# [0.6.0](https://gitlab.com/memorize_it/memorize/compare/v0.5.0...v0.6.0) (2020-08-07)

**Note:** Version bump only for package @memorize/cli





# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)

**Note:** Version bump only for package @memorize/cli





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)

**Note:** Version bump only for package @memorize/cli





## [0.3.1](https://gitlab.com/memorize_it/memorize/compare/v0.3.0...v0.3.1) (2020-02-08)


### Bug Fixes

* **types:** use @memorize/types as dependency to allow use of enums,maps... on runtime ([58c64b0](https://gitlab.com/memorize_it/memorize/commit/58c64b06160c105ac9073e82de8b4d087883fe68))





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)


### Features

* **cli:** Allow to generate zettelkasten svg visualization and update aliases ([992b5a8](https://gitlab.com/memorize_it/memorize/commit/992b5a8b9e7586957530998592c9741c0540750c))
* **cli:** ask for diagram type when creating visualization svg ([cb97561](https://gitlab.com/memorize_it/memorize/commit/cb975614af8e29c2b353859afb63bde9bc37ff65))
* **cli:** make tag suggestion when similar tags  are introduced ([1c7a2d5](https://gitlab.com/memorize_it/memorize/commit/1c7a2d594a732bb46d185c6bcb0866eddac472ff))





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **zettelkasten:** initialize a zettelkasten on target directory. ([b8f810c](https://gitlab.com/memorize_it/memorize/commit/b8f810cdf6d1f60cce4a41f846ac906ff53dd0c2))
* **zettel:** add zettel from the cli ([012c4bd](https://gitlab.com/memorize_it/memorize/commit/012c4bd66439118af178c445be208358c9a7696c))





## [0.1.1](https://gitlab.com/memorize_it/memorize/compare/v0.1.0...v0.1.1) (2020-01-18)

**Note:** Version bump only for package @memorize/cli





# 0.1.0 (2020-01-18)


### Features

* **cli:** add cli with create id feature ([02fa676](https://gitlab.com/memorize_it/memorize/commit/02fa67638c6745cea73d2026e187b9b9980102bc))
