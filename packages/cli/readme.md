# Memorize - Cli

📓 MemorizeCli is a command line utility to be used tocapture information in an structured way.

## Install

`npm i -g @memorize/cli`

## Commands

### Generate Id

Command: `memorize id`

Generates an ID. It's based on current time, for example, the ID corresponding to 1970-01-01 15:00:0 is `19700101150000`.

### Create new zettel

Command: `memorize addZettel`

After answering some questions like title, path, or tags, a new markdown file is created respecting the @memorize/schema.

### Create visualization

Command: `memorize print`

Creates an svg file with the graphic representation of the zettelkasten.
