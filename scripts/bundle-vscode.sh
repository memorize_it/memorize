#!/bin/bash
function rename {
	search='("name":[[:space:]]*").+(")'
	replace="\1memorize\2"

    sed -i -E "s/${search}/${replace}/g" "$1"

    search2='("main":[[:space:]]*").+(")'
    replace2="\1\.\/out\/extension\.js\2"

    sed -i -E "s/${search2}/${replace2}/g" "$1"
}

cd ./packages/vscode
rename "package.json"
yarn vsce package --baseContentUrl " " --baseImagesUrl " " --yarn
PACKAGE_VERSION=$(cat package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[\",]//g' | tr -d '[[:space:]]')
yarn vsce publish $PACKAGE_VERSION -p $AZURE_TOKEN  --baseContentUrl " " --baseImagesUrl " " --yarn
