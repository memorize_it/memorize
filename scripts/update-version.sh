#!/bin/bash

function replaceVersion() {
    search="(version[[:space:]]=[[:space:]]').+(')"
    replace="\1${1}\2"

    sed -i -E "s/${search}/${replace}/g" "$2"
}

PACKAGE_VERSION=$(cat ./packages/core/package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[\",]//g' | tr -d '[[:space:]]')
FILES_PATH=packages/**/**/version.ts
FILES=$(find ${FILES_PATH})

for FILE in $FILES; do
    replaceVersion $PACKAGE_VERSION $FILE
done

git add .
