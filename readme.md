# Memorize

📓 Memorize is a set of tools that allows you to use markdown files as a Zettelkasten note-taking system.

## Background

The idea behind Memorize :

- Use regular markdown files.
- Respect a minimum schema. No more than 10 convention to be respected.
- Do not depend on any specific software to **edit** the files. Use the editor of your choice.
- Provide tools that check the files and help to respect the conventions.
- Provide tools to explore the information in an interactive way.

## 💡 How Memorize works :

Memorize build a zettelkasten interpreting all the Markdown files inside a directory.

It will parse both, the front-matter (in `yaml`) and the content of the file.

For example, given this file :

```markdown
---
id: 20200224175325
tags:
  - tag
  - example_tag
references: []
---
# Example zettle

My markdown #IsCool
```

Memorize would build the following object :

```json
{
    "id": "20200120160015",
    "references": [],
    "tags": ["tag", "example_tag", "iscool"],
    "title": "Example zettle",
}
```

If for example, you want to make a link between two zettels you just need to include the zettelId inside the markdown body or in the references array.

For example, given this new file :

```markdown
---
id: 20200224175326
tags:
  - tag
references: 
  - 20200120160010
---
# Second zettle

- [Example zettel](20200120160015)
```

Memorize would build the following object :

```json
{
    "id": "20200120160016",
    "references": ["20200120160010", "20200120160015"],
    "tags": ["tag"],
    "title": "Second zettle",
}
```

## Modules

### User applications

There are currently two applications in developement which allow you to use markdown files following the Memorize schema as a zettelkasten.

Cli and VSCode extensions intend to support you while capturing information in an structured way. The VSCode extension also allows you to visualize a graph of the zettelkasten.

### Core modules

These modules include all the logic behind Memorize :

- core: Internal logics of Memorize, analyse zettels, parse tags, create ids... 
- file-system: deal with fs in order to read markdown files and create a zettel object from them so that Memorize can work with it 
- language: natural language processing related methods
- types: types, interfaces and constant shared between the multiple packages
- utils: modifications to third-party libraries to fit Memorize needs
- visualization: create zettelkasten visualizations
