# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.9.1](https://gitlab.com/memorize_it/memorize/compare/v0.9.0...v0.9.1) (2021-04-18)


### Bug Fixes

* **visualization:** throw error only if target index is undefined ([7fcfc0b](https://gitlab.com/memorize_it/memorize/commit/7fcfc0b36792b384f893d9b3e4d4aa1697c8ddd5))





# [0.9.0](https://gitlab.com/memorize_it/memorize/compare/v0.8.1...v0.9.0) (2021-04-18)


### Bug Fixes

* **file-system:** load filesWithErrors when loading a new zettelkasten ([be020ef](https://gitlab.com/memorize_it/memorize/commit/be020ef114d83c3eb359d97060f75feeb89c3a03))
* **file-system:** on generateZettelsForEmptyLinksInZettel complete only empty links ([9692d04](https://gitlab.com/memorize_it/memorize/commit/9692d04360a6b9b75a1c5f7ffa498056c43ea405))
* **file-system:** preserve existing zettel format when creating missing zettels ([83d9400](https://gitlab.com/memorize_it/memorize/commit/83d9400c7a72ec2e4feaf8391527e732c23e1456))


### Features

* **cli:** read -s option to save or not zettelkasten file ([985c63b](https://gitlab.com/memorize_it/memorize/commit/985c63b14dd88e0327863c5ab8dc2942f5f110e2))
* **core:** check for zettel errors on create zettelkasten ([053b091](https://gitlab.com/memorize_it/memorize/commit/053b0917c46111cc2520ad9273d38a2e18c47928))
* **core:** replace tagson markdown with links ([650559d](https://gitlab.com/memorize_it/memorize/commit/650559d9a22495f1c03d572117c081397037faab))
* **file-system:** extract selection to new zettel ([1200a89](https://gitlab.com/memorize_it/memorize/commit/1200a895e83ad79a25593548bf791c104a4523d5))
* **file-system:** user must specify if it wants to save zettelkasten file ([3e03ac3](https://gitlab.com/memorize_it/memorize/commit/3e03ac36b98b74b596892835f464c09702548ab8))
* **types:** add zettels with error to zettelkasten metadata ([389835d](https://gitlab.com/memorize_it/memorize/commit/389835d876fd38748dacaeaf21a8e66985192b55))
* **vscode:** extract selection to new zettel ([c7bdb3e](https://gitlab.com/memorize_it/memorize/commit/c7bdb3e63d9c4649c0a5f0f1cf10ce7519c2e414))
* **vscode:** let user configure if zettelkasten file is saved ([16e1fae](https://gitlab.com/memorize_it/memorize/commit/16e1fae5df8b8b0a19e11e00e720db1d23c8e920))





## [0.8.1](https://gitlab.com/memorize_it/memorize/compare/v0.8.0...v0.8.1) (2020-12-30)


### Bug Fixes

* **file-system:** handle short path cases ([e410bee](https://gitlab.com/memorize_it/memorize/commit/e410beef1a45833990d3629641d363d86961fd47))





# [0.8.0](https://gitlab.com/memorize_it/memorize/compare/v0.7.2...v0.8.0) (2020-12-27)


### Features

* **core:** add files with error to zettelkasten ([f89f9ef](https://gitlab.com/memorize_it/memorize/commit/f89f9ef4dc8f9bc5e00acf52142d0af29d90e059))
* **core:** find zettel in zettelkasten ([8285bf9](https://gitlab.com/memorize_it/memorize/commit/8285bf92e4902ab91c96e8538800aa766667acfc))
* **file-system:** when loading the zettelkasten log all the files with errors ([faae390](https://gitlab.com/memorize_it/memorize/commit/faae390736645e9c8b2aac7bc1f548e4c753646d))
* **file-system:** zettel id to path ([bc7c19c](https://gitlab.com/memorize_it/memorize/commit/bc7c19cc4268e19a018c179b8663da9972f8054f))
* **types:** add error files to zettelkasten meta ([776a361](https://gitlab.com/memorize_it/memorize/commit/776a361dfc60eaa812b4526e2b41bd3aa7c5f3a8))
* **vscode:** display and log errors ([2d29b52](https://gitlab.com/memorize_it/memorize/commit/2d29b5235ea3e4b3d1de80c0a5b43e805b0a42a4))





## [0.7.2](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.2) (2020-09-21)


### Bug Fixes

* **cli:** add extension to file when creating new zettel ([f2b78af](https://gitlab.com/memorize_it/memorize/commit/f2b78af0c77b84e24238e6ab894ca48501d1d814))


### Features

* **cli:** dynamic version update ([70c5090](https://gitlab.com/memorize_it/memorize/commit/70c5090730613e741772e377ae4d628da147558d))
* **core:** add version number to zettelkasten ([4eed9f3](https://gitlab.com/memorize_it/memorize/commit/4eed9f3cd21990f69669d4019c19679529df5c54))
* **core:** replace links on zettels, create zettelkasten and get relatedZettels ([6befa40](https://gitlab.com/memorize_it/memorize/commit/6befa40403a8fea8c431c237769a406947ef6b67))
* **file-system:** add zettel header to file ([cea86c0](https://gitlab.com/memorize_it/memorize/commit/cea86c0e0641555e512c79482e9ce4dcc4aa1d68))
* **file-system:** generate files for empty links ([d27a340](https://gitlab.com/memorize_it/memorize/commit/d27a340ab746ade2af9489de737b4b7c70c67202))
* **file-system:** load full zettelkasten if version doesn't match ([81a210d](https://gitlab.com/memorize_it/memorize/commit/81a210dffc9ff23c03be581a9cb7edfef549139d))
* **file-system:** retrieve zettel with content ([9e2c1b9](https://gitlab.com/memorize_it/memorize/commit/9e2c1b938e8c6dae8c536902dbe6eff3bfbd7bba))
* **types:** add optional content to zettel ([5263dc5](https://gitlab.com/memorize_it/memorize/commit/5263dc52cc8afe6585e4ad9275db9c0fb10cde16))
* **types:** add version to zettelkasten meta ([c113520](https://gitlab.com/memorize_it/memorize/commit/c11352018ac5c2b9a2b0621e911ef4d5937fefd3))
* **types:** specify zettel metadata ([48f28d2](https://gitlab.com/memorize_it/memorize/commit/48f28d24cceca4816bf8887f2d3df1aeb445d69f))
* **utils:** add method to remove file ([99c497d](https://gitlab.com/memorize_it/memorize/commit/99c497df1ab47be6c837629e435add41c5bd5c67))
* **utils:** add new promisified methods for fs ([03dba15](https://gitlab.com/memorize_it/memorize/commit/03dba155243eb2242956056b7ccf50bcacdfd96a))
* **visualization:** allow to get zettelkasten spec directly ([d828964](https://gitlab.com/memorize_it/memorize/commit/d8289640102e2d30dd14b8684785a2f6415832d7))
* **visualization:** emit zettel node on click ([c0fc899](https://gitlab.com/memorize_it/memorize/commit/c0fc899e3be64ac30eac163a8cf2e7f2ed348973))
* **visualization:** set node groups based on first tag on the file ([5420c8a](https://gitlab.com/memorize_it/memorize/commit/5420c8ab8b1688cdf5a1964fe71e087fb528ecb9))
* **vscode:** add zettel header on current file ([fa8655e](https://gitlab.com/memorize_it/memorize/commit/fa8655e528b2155ecdee9294f13eff068aa5a17d))
* **vscode:** create zettels for empty links ([51bb00d](https://gitlab.com/memorize_it/memorize/commit/51bb00d30e794d192045b240f5331e79938d5d2d))
* **vscode:** new command to Open Zettel given an ID or path ([d793c37](https://gitlab.com/memorize_it/memorize/commit/d793c37cca66242bd321ffff7c6b9334f69e830e))
* **vscode:** on the visualization open zettel on click ([9ade7dd](https://gitlab.com/memorize_it/memorize/commit/9ade7dd89f0fcf2253ec9b0b53c6d91c3a31f2c7))
* **vscode:** set background color for the visualization and default to white ([5af049b](https://gitlab.com/memorize_it/memorize/commit/5af049b3876de7240e1f95b74a56cef0ec5e211b))





## [0.7.1](https://gitlab.com/memorize_it/memorize/compare/v0.6.0...v0.7.1) (2020-09-21)


### Bug Fixes

* **cli:** add extension to file when creating new zettel ([f2b78af](https://gitlab.com/memorize_it/memorize/commit/f2b78af0c77b84e24238e6ab894ca48501d1d814))


### Features

* **cli:** dynamic version update ([70c5090](https://gitlab.com/memorize_it/memorize/commit/70c5090730613e741772e377ae4d628da147558d))
* **core:** add version number to zettelkasten ([4eed9f3](https://gitlab.com/memorize_it/memorize/commit/4eed9f3cd21990f69669d4019c19679529df5c54))
* **core:** replace links on zettels, create zettelkasten and get relatedZettels ([6befa40](https://gitlab.com/memorize_it/memorize/commit/6befa40403a8fea8c431c237769a406947ef6b67))
* **file-system:** add zettel header to file ([cea86c0](https://gitlab.com/memorize_it/memorize/commit/cea86c0e0641555e512c79482e9ce4dcc4aa1d68))
* **file-system:** generate files for empty links ([d27a340](https://gitlab.com/memorize_it/memorize/commit/d27a340ab746ade2af9489de737b4b7c70c67202))
* **file-system:** load full zettelkasten if version doesn't match ([81a210d](https://gitlab.com/memorize_it/memorize/commit/81a210dffc9ff23c03be581a9cb7edfef549139d))
* **file-system:** retrieve zettel with content ([9e2c1b9](https://gitlab.com/memorize_it/memorize/commit/9e2c1b938e8c6dae8c536902dbe6eff3bfbd7bba))
* **types:** add optional content to zettel ([5263dc5](https://gitlab.com/memorize_it/memorize/commit/5263dc52cc8afe6585e4ad9275db9c0fb10cde16))
* **types:** add version to zettelkasten meta ([c113520](https://gitlab.com/memorize_it/memorize/commit/c11352018ac5c2b9a2b0621e911ef4d5937fefd3))
* **types:** specify zettel metadata ([48f28d2](https://gitlab.com/memorize_it/memorize/commit/48f28d24cceca4816bf8887f2d3df1aeb445d69f))
* **utils:** add method to remove file ([99c497d](https://gitlab.com/memorize_it/memorize/commit/99c497df1ab47be6c837629e435add41c5bd5c67))
* **utils:** add new promisified methods for fs ([03dba15](https://gitlab.com/memorize_it/memorize/commit/03dba155243eb2242956056b7ccf50bcacdfd96a))
* **visualization:** allow to get zettelkasten spec directly ([d828964](https://gitlab.com/memorize_it/memorize/commit/d8289640102e2d30dd14b8684785a2f6415832d7))
* **visualization:** emit zettel node on click ([c0fc899](https://gitlab.com/memorize_it/memorize/commit/c0fc899e3be64ac30eac163a8cf2e7f2ed348973))
* **visualization:** set node groups based on first tag on the file ([5420c8a](https://gitlab.com/memorize_it/memorize/commit/5420c8ab8b1688cdf5a1964fe71e087fb528ecb9))
* **vscode:** add zettel header on current file ([fa8655e](https://gitlab.com/memorize_it/memorize/commit/fa8655e528b2155ecdee9294f13eff068aa5a17d))
* **vscode:** create zettels for empty links ([51bb00d](https://gitlab.com/memorize_it/memorize/commit/51bb00d30e794d192045b240f5331e79938d5d2d))
* **vscode:** new command to Open Zettel given an ID or path ([d793c37](https://gitlab.com/memorize_it/memorize/commit/d793c37cca66242bd321ffff7c6b9334f69e830e))
* **vscode:** on the visualization open zettel on click ([9ade7dd](https://gitlab.com/memorize_it/memorize/commit/9ade7dd89f0fcf2253ec9b0b53c6d91c3a31f2c7))
* **vscode:** set background color for the visualization and default to white ([5af049b](https://gitlab.com/memorize_it/memorize/commit/5af049b3876de7240e1f95b74a56cef0ec5e211b))





# [0.6.0](https://gitlab.com/memorize_it/memorize/compare/v0.5.0...v0.6.0) (2020-08-07)


### Bug Fixes

* **jest:** update roots to target only src ([a0715ff](https://gitlab.com/memorize_it/memorize/commit/a0715ffb004b32a785ac7cf51cab44b21bc8fa26))


### Features

* **core:** specify character to replace space when sanitizing text ([88d1003](https://gitlab.com/memorize_it/memorize/commit/88d1003752d5c82e71895f347924b9c7babd60f4))
* **file-system:** return fillepath on write ([8c80af3](https://gitlab.com/memorize_it/memorize/commit/8c80af37f64ba4b05f524b95d79b5afc6a263a8c))
* **vscode:** after creatingnew zettel open file and move to EOF ([10c7f95](https://gitlab.com/memorize_it/memorize/commit/10c7f95c5a8d60d73efaa29044ded95e7aa84442))
* **vscode:** use extensions setting to define a character to replace space on filename while creati ([032b65c](https://gitlab.com/memorize_it/memorize/commit/032b65c9fe8135d984d0ed08afbac08e7dab4826))





# [0.5.0](https://gitlab.com/memorize_it/memorize/compare/v0.4.0...v0.5.0) (2020-02-24)


### Features

* **vscode:** add new zettel ([9e6514d](https://gitlab.com/memorize_it/memorize/commit/9e6514d7454334300648d95a8db986fce2d7c9fc))





# [0.4.0](https://gitlab.com/memorize_it/memorize/compare/v0.3.1...v0.4.0) (2020-02-15)


### Features

* **core:** add method to zettelkasten to consolidate tags ([9ad3e12](https://gitlab.com/memorize_it/memorize/commit/9ad3e129f9502ff7baa7d0920f53ca5b5e1f8ef3))
* **file-system:** addtags to metadata when loading zettelkasten ([33db0c6](https://gitlab.com/memorize_it/memorize/commit/33db0c62cc6a69d9fb0c310959ca14ac03e0fa4b))
* **types:** add tagsto zettelkasten metadata ([29b5e18](https://gitlab.com/memorize_it/memorize/commit/29b5e18162aea533b2d03e372051b047cf29ea6c))
* **vscode:** add tag and link completion ([57bb437](https://gitlab.com/memorize_it/memorize/commit/57bb437b615d0627aefff2716817b0af945fff2a))
* **vscode:** bootstrap vscode extension with generateId ([29f7e22](https://gitlab.com/memorize_it/memorize/commit/29f7e222c082a740f4bc74ec1ba85344d5630f0f))
* **vscode:** follow zettel id link ([d8d6235](https://gitlab.com/memorize_it/memorize/commit/d8d623582e9da28e05641efde57a4ea22cfe0c49))
* **vscode:** visualize  zettelkasten as svg ([e1228f9](https://gitlab.com/memorize_it/memorize/commit/e1228f9cb7ffc6c88e284277caa8ba4afb10f70c))





## [0.3.1](https://gitlab.com/memorize_it/memorize/compare/v0.3.0...v0.3.1) (2020-02-08)


### Bug Fixes

* **types:** use @memorize/types as dependency to allow use of enums,maps... on runtime ([58c64b0](https://gitlab.com/memorize_it/memorize/commit/58c64b06160c105ac9073e82de8b4d087883fe68))





# [0.3.0](https://gitlab.com/memorize_it/memorize/compare/v0.2.0...v0.3.0) (2020-02-08)


### Bug Fixes

* **core:** move regeps from types to core. They are used on the runtime !!! XD ([c613128](https://gitlab.com/memorize_it/memorize/commit/c6131288b5c50d6962e8c59ee6f4df03a4d4c60d))


### Features

* **cli:** Allow to generate zettelkasten svg visualization and update aliases ([992b5a8](https://gitlab.com/memorize_it/memorize/commit/992b5a8b9e7586957530998592c9741c0540750c))
* **cli:** ask for diagram type when creating visualization svg ([cb97561](https://gitlab.com/memorize_it/memorize/commit/cb975614af8e29c2b353859afb63bde9bc37ff65))
* **cli:** make tag suggestion when similar tags  are introduced ([1c7a2d5](https://gitlab.com/memorize_it/memorize/commit/1c7a2d594a732bb46d185c6bcb0866eddac472ff))
* **core:** allow to search for similar  tags already exisitng ([984c478](https://gitlab.com/memorize_it/memorize/commit/984c47881fa4a32499fcb84f31bc07d44e03d7d3))
* **core:** wHen parsing similartags don't make any suggestion if tag already exists ([ae843b9](https://gitlab.com/memorize_it/memorize/commit/ae843b97e913167e99e14dc5d8a6cf853d14de16))
* **file-system:** add diagram type to zettelkasten visualization ([ca5e736](https://gitlab.com/memorize_it/memorize/commit/ca5e736a0ee61b587896c9c61aa9e74a8e260ca2))
* **file-system:** create svg file with zettelkasten visualization ([3a13c93](https://gitlab.com/memorize_it/memorize/commit/3a13c9349073d444eac024fcd221b19adf56730c))
* **language:** jaro and jaro-winkler distances ([f2dbfd7](https://gitlab.com/memorize_it/memorize/commit/f2dbfd7080022142ba96d14f6c3948d4310a5095))
* **types:** define diagram types ([a67c142](https://gitlab.com/memorize_it/memorize/commit/a67c142bab52a6a1c09d6e3445dbd8aba134b076))
* **visualization:** force diagram node size depends on times referenced ([6d3e787](https://gitlab.com/memorize_it/memorize/commit/6d3e787f231ace0e808669adcfa825ae2b7cce2a))
* **visualization:** force diagram visualization ([76a74d4](https://gitlab.com/memorize_it/memorize/commit/76a74d446b83118ff85032e50395b1273ef1e8b8))
* **visualization:** generate arc diagram ([693518c](https://gitlab.com/memorize_it/memorize/commit/693518cab16259634a3ceaee44ba607a8d5d7d46))





# [0.2.0](https://gitlab.com/memorize_it/memorize/compare/v0.1.1...v0.2.0) (2020-01-20)


### Features

* **file-system:** add basic file-system operations ([123c5f0](https://gitlab.com/memorize_it/memorize/commit/123c5f091c74352a7f80199abd185863021f3ef2))
* **file-system:** parse zettel from file and zettel to file ([e24a35d](https://gitlab.com/memorize_it/memorize/commit/e24a35df0b1160480021f1e0620b72bf8b85e476))
* **graph:** zettelkasten to graph funtion ([41df447](https://gitlab.com/memorize_it/memorize/commit/41df447ce26cd8d4220bd5815a40c1054e21edf9))
* **types:** add new types module ([ea1f418](https://gitlab.com/memorize_it/memorize/commit/ea1f418e4bba629ca5172f7b1d32ed42bafb7d59))
* **utils:** add new utils module ([ebb2bbb](https://gitlab.com/memorize_it/memorize/commit/ebb2bbb6403433c697c1c3487e0bf3838d21624f))
* **zettelkasten:** initialize a zettelkasten on target directory. ([b8f810c](https://gitlab.com/memorize_it/memorize/commit/b8f810cdf6d1f60cce4a41f846ac906ff53dd0c2))
* **zettel:** add sanitize  text method ([fb0cfcf](https://gitlab.com/memorize_it/memorize/commit/fb0cfcf7c35a342d4494c24adf89da79038217ec))
* **zettel:** add zettel from the cli ([012c4bd](https://gitlab.com/memorize_it/memorize/commit/012c4bd66439118af178c445be208358c9a7696c))
* **zettel:** differentiate zettelbody and metadata ([dbd07a4](https://gitlab.com/memorize_it/memorize/commit/dbd07a4f76057e1aa3b21ab53dc5c49c9984adae))





## [0.1.1](https://gitlab.com/memorize_it/memorize/compare/v0.1.0...v0.1.1) (2020-01-18)

**Note:** Version bump only for package memorize





# 0.1.0 (2020-01-18)


### Features

* **cli:** add cli with create id feature ([02fa676](https://gitlab.com/memorize_it/memorize/commit/02fa67638c6745cea73d2026e187b9b9980102bc))
* **core:** create id from date ([85d59c2](https://gitlab.com/memorize_it/memorize/commit/85d59c2a74173d1c3ed224c9e9ee1b7295d3ed4c))
